.PHONY: format
format:
	@poetry run isort .
	@poetry run black .

.PHONY: ci
ci:
	@poetry run isort . --check
	@poetry run black . --check
	@poetry run mypy src/

.PHONY: test
test:
	@poetry run pytest

.PHONY: docs
docs:
	@poetry run marimo -y -q export html-wasm docs/source/tools/notebooks/validate_authnrequest_signature.py -o docs/source/_static/notebooks/validate_authnrequest_signature --mode run
	@poetry run make -C docs all
