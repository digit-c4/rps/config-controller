import marimo

__generated_with = "0.11.17"
app = marimo.App(
    width="medium",
    app_title="Validate SAML AuthnRequest Signature",
)


@app.cell
def _(mo):
    mo.md(
        r"""
        # Validate SAML AuthnRequest Signature

        This notebook was created in order to help you to valiate a AuthnRequest signature. This can be usefull in case when the Identity Provider (IdP) doesn't validate the signature of the AuhtnRequest forwarded by the Service Provider (SP) and we want to know if our setup is well done.

        Please provide the AuhtnRequest in base64 format directly copied in the `SAMLRequest` parameter from POST request send by the principal (the user agent) to the IdP.

        The certificate must be provided in PEM format.
        """
    )
    return


@app.cell
def _():
    import marimo as mo

    return (mo,)


@app.cell
def _(mo):
    certificate = mo.ui.text_area(value="", full_width=True, rows=25)
    authnrequest = mo.ui.text_area(value="", full_width=True, rows=10)

    mo.md(
        f"""
        Authnrequest in base64: {authnrequest}
        Certificate in PEM format: {certificate}
        """
    )
    return authnrequest, certificate


@app.cell
def _(authnrequest, certificate, mo):
    import base64

    from cryptography import x509
    from cryptography.hazmat.primitives import hashes
    from cryptography.hazmat.primitives.asymmetric import padding
    from lxml import etree

    def get_public_key_from_certificate(pem):
        return x509.load_pem_x509_certificate(str.encode(pem)).public_key()

    def extract_signature_and_signed_info(xml_data):
        ns = {"ds": "http://www.w3.org/2000/09/xmldsig#"}
        tree = etree.fromstring(xml_data)

        signature_element = tree.find(".//ds:Signature", namespaces=ns)
        if signature_element is None:
            raise ValueError("Signature non trouvée dans le document XML.")

        signed_info_element = signature_element.find("ds:SignedInfo", namespaces=ns)
        if signed_info_element is None:
            raise ValueError("SignedInfo non trouvé dans la Signature.")

        signed_info_canon = etree.tostring(
            signed_info_element, method="c14n", exclusive=True
        )

        signature_value_element = signature_element.find(
            "ds:SignatureValue", namespaces=ns
        )
        if signature_value_element is None:
            raise ValueError("SignatureValue non trouvé dans la Signature.")

        signature_value = base64.b64decode(signature_value_element.text.strip())

        return signed_info_canon, signature_value

    def verify_xml_signature(xml_data, public_key_pem):
        public_key = get_public_key_from_certificate(public_key_pem)
        signed_info, signature_value = extract_signature_and_signed_info(xml_data)

        try:
            public_key.verify(
                signature_value, signed_info, padding.PKCS1v15(), hashes.SHA256()
            )
            return True
        except Exception as e:
            return False

    is_valid = False

    if len(authnrequest.value.strip()) > 0 and len(certificate.value.strip()) > 0:
        is_valid = verify_xml_signature(
            base64.b64decode(authnrequest.value.strip()), certificate.value.strip()
        )

    mo.md(f"Signature validity: **{is_valid}**.")
    return (
        base64,
        etree,
        extract_signature_and_signed_info,
        get_public_key_from_certificate,
        hashes,
        is_valid,
        padding,
        verify_xml_signature,
        x509,
    )


if __name__ == "__main__":
    app.run()
