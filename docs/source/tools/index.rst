Tools
=====

Here you can find the different tools we provide in order to help you to develop or troubleshot.

* `Validate SAML Authnrequest Signature <../_static/notebooks/validate_authnrequest_signature>`_

If you want to contribute, please start `marimo <https://marimo.io/>`_ and you can update or create new tools.

Start marimo at the root of the project:

``marimo edit docs/source/tools/notebooks/``
