Architecture
============

Configuration
-------------

.. csv-table::
   :header: Environment Variable, Default Value, Description
   :widths: 20, 20, 60

   ``DEBUG``, ``False``, "Enable debug mode"
   ``PORT``, ``8000``, "Port to run the API on"
   ``NETBOX_API``, N/A, "Netbox URL"
   ``NETBOX_TOKEN``, N/A, "Netbox authentication token (with write access)"
   ``EC_RPS_ALLOWED_ORIGINS``, ``*``, "Allowed IP networks"
   ``EC_RPS_CERTS_PATH``, N/A, "Path to folder containing X.509 certificates"
   ``EC_RPS_WEBHOOK_SCOPE``, N/A, "Webhook name identifier in Netbox (i.e. ``waf``, ``rps``, ...)"
   ``EC_BUBBLE``, N/A, "Name of the bubble this project is running in"
   ``EC_ENV``, N/A, "Name of the environment within the bubble this project is running in"
   ``EC_RPS_CONFIG_MODULE``, N/A, "Path to the configuration module to use"

Available configuration modules are:

* ``ec_rps_config_proxy_nginx``: Configures NGINX+ Reverse Proxy
* ``ec_rps_config_waf_modsecurity``: Configures ModSecurity WAF with Apache webserver

``ec_rps_config_proxy_nginx`` configuration
-------------------------------------------

.. csv-table::
   :header: Environment Variable, Default Value, Description
   :widths: 20, 20, 60

   ``EC_RPS_NGINX_PLUS_ENABLED``, ``False``, "Wether NGINX+ features are enabled"
   ``EC_RPS_ACME_ENABLED``, ``True``, "Wether certificates are generated via ACME challenge"
   ``EC_RPS_SAML_ENABLED``, ``True``, "Wether SAML configuration should be generated"

**SAML related configuration (ignored if ``EC_RPS_SAML_ENABLED`` or ``EC_RPS_NGINX_PLUS_ENABLED`` are false):**

.. csv-table::
   :header: Environment Variable, Default Value, Description
   :widths: 20, 20, 60

   ``EC_RPS_SAML_SP_ENTITY_ID``, N/A, "Unique identifier for the Service Provider"
   ``EC_RPS_SAML_SP_ACS_URL``, N/A, "URL for the Service Provider’s Assertion Consumer Service"
   ``EC_RPS_SAML_SP_REQUEST_BINDING``, ``HTTP-POST``, "SAML request binding method (e.g., HTTP-POST)"
   ``EC_RPS_SAML_SP_SIGN_AUTHN``, ``False``, "Whether to sign authentication requests"
   ``EC_RPS_SAML_SP_SIGNING_KEY``, ``""``, "PEM-encoded private key used for signing SAML requests"
   ``EC_RPS_SAML_SP_DECRYPTION_KEY``, ``""``, "PEM-encoded private key for decrypting SAML assertions"
   ``EC_RPS_SAML_SP_FORCE_AUTHN``, ``False``, "Whether to force the Identity Provider to re-authenticate"
   ``EC_RPS_SAML_SP_NAMEID_FORMAT``, ``urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified``, "Format of the NameID in SAML assertions"
   ``EC_RPS_SAML_SP_RELAY_STATE``, ``""``, "Custom state parameter included with SAML messages"
   ``EC_RPS_SAML_SP_WANT_SIGNED_RESPONSE``, ``False``, "Whether the SAML response must be signed"
   ``EC_RPS_SAML_SP_WANT_SIGNED_ASSERTION``, ``False``, "Whether the SAML assertion must be signed"
   ``EC_RPS_SAML_SP_WANT_ENCRYPTED_ASSERTION``, ``False``, "Whether the SAML assertion must be encrypted"
   ``EC_RPS_SAML_SP_SLO_URL``, N/A, "Service Provider Single Logout endpoint URL"
   ``EC_RPS_SAML_SP_SLO_BINDING``, ``HTTP-POST``, "Binding method for Single Logout requests"
   ``EC_RPS_SAML_SP_SIGN_SLO``, ``False``, "Whether to sign Single Logout requests"
   ``EC_RPS_SAML_SP_WANT_SIGNED_SLO``, ``False``, "Whether Single Logout messages must be signed"
   ``EC_RPS_SAML_IDP_ENTITY_ID``, N/A, "Unique identifier for the Identity Provider"
   ``EC_RPS_SAML_IDP_SSO_URL``, N/A, "URL for the Identity Provider's Single Sign-On endpoint"
   ``EC_RPS_SAML_IDP_VERIFICATION_CERTIFICATE``, ``""``, "PEM-encoded certificate to verify the Identity Provider's signature"
   ``EC_RPS_SAML_IDP_SLO_URL``, N/A, "Identity Provider Single Logout endpoint URL"
   ``EC_RPS_SAML_SP_REDIRECT_BASE``, N/A, "URL the Identity Provider should redirect to after successful login"
   ``EC_RPS_SAML_AUTH_HEADER_NAME``, ``Remote-User``, "HTTP header sent to the upstream containing the authenticated user identifier"

``ec_rps_config_waf_modsecurity`` configuration
-----------------------------------------------

.. csv-table::
   :header: Environment Variable, Default Value, Description
   :widths: 20, 20, 60

   ``EC_RPS_ACME_ENABLED``, ``True``, "Wether certificates are generated via ACME challenge"
   ``EC_RPS_PROXY_HOST``, N/A, "Hostname of the Reverse Proxy to redirect requests to"

Schema
------

.. diagrams:: diagram.py
