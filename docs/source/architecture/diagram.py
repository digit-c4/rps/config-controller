from unittest.mock import patch
import html
import sys
import textwrap

from diagrams import Diagram
from diagrams.c4 import C4Node, Database, Relationship, System, SystemBoundary


def _format_description(description):
    wrapper = textwrap.TextWrapper(width=30, max_lines=3)
    lines = [html.escape(line) for line in wrapper.wrap(description)]
    lines += [""] * (3 - len(lines))  # fill up with empty lines so it is always three
    return "<br/>".join(lines)


def Component(name, technology="", description="", **kwargs):
    container_attributes = {
        "name": name,
        "technology": technology,
        "description": description,
        "type": "Component",
        "fillcolor": "lightskyblue",
        "fontcolor": "black",
    }
    container_attributes.update(kwargs)
    return C4Node(**container_attributes)


with patch("diagrams.c4._format_description", new=_format_description):
    with Diagram("Architecture", direction="TB", filename=sys.argv[1], show=False):
        netbox = System(
            "Netbox",
            description="Contains RPS mappings",
            external=True,
        )

        with SystemBoundary("Config Controller"):
            api = Component(
                "API",
                technology="FastAPI",
                description="Receives RPS mappings via Netbox webhooks",
            )

            controller = Component(
                "Controller",
                technology="Async Task",
                description="Orchestrates other components",
            )

            filewatcher = Component(
                "File Watcher",
                technology="Async Task",
                description="Detects when certificates are created or renewed",
            )

            state_manager = Component(
                "State Manager",
                technology="Async Task",
                description="Keeps RPS mappings in memory, generates configuration and reloads webserver",
            )

            filesystem = Database(
                "Local Filesystem",
                description="Contains X.509 certificates and generated configuration",
            )

        netbox >> Relationship("Webhook") >> api
        api >> Relationship("Notify") >> controller
        controller >> Relationship("Orchestrate") >> state_manager
        filewatcher >> Relationship("Notify") >> state_manager
        filewatcher >> Relationship("Watch") >> filesystem
        state_manager >> Relationship("Write") >> filesystem
