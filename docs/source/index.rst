RPS - Config Controller: Documentation
======================================

.. image:: https://code.europa.eu/digit-c4/rps/config-controller/badges/main/pipeline.svg?style=flat-square
   :target: https://code.europa.eu/digit-c4/rps/config-controller/-/commits/main
   :alt: Pipeline Status

.. image:: https://code.europa.eu/digit-c4/rps/config-controller/-/badges/release.svg?style=flat-square
   :target: https://code.europa.eu/digit-c4/rps/config-controller/-/releases
   :alt: Latest Release

Introduction
------------

You will find here information about the RPS Config Controller, a tool used to
generate configuration files from Netbox RPS Mappings.

Content
-------

.. toctree::
   :maxdepth: 2

   architecture/index
   api/index
   tools/index
