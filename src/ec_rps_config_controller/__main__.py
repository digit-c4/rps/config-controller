"""
This module is the entrypoint for the Config Controller. It is responsible for
setting up the logging and starting the main worker task.
"""

import sys

import logbook
import trio

from . import worker
from .logging import create_loghandler


def main() -> None:
    shutdown_handler = worker.ShutdownHandler()
    shutdown_handler.connect()

    with create_loghandler().applicationbound():
        try:
            trio.run(worker.task, shutdown_handler.wait)

        except Exception:
            logbook.exception("Uncaught error")
            sys.exit(1)


main()
