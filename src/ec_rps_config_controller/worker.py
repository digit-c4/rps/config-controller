"""
This module is the main worker task for the Config Controller. It is responsible
for starting the controller and the API server, and setting up the webhook.
"""

from typing import Any, Awaitable, Callable

import signal

import trio

from ec_rps_config_controller import api, sync
from ec_rps_config_controller.business.webhook import setup_webhook
from ec_rps_config_controller.sync.abc import ControllerChannelWriter


async def task(shutdown_trigger: Callable[..., Awaitable[None]]) -> None:
    """
    Main worker task.

    :param shutdown_trigger:
        Function which waits until a graceful shutdown is requested.
    """

    async with trio.open_nursery() as nursery:
        tx: ControllerChannelWriter = await nursery.start(sync.task)  # type: ignore[assignment]
        await nursery.start(api.asgi_app, tx, shutdown_trigger)
        await setup_webhook()


class ShutdownHandler:
    """
    Setup a signal handler to trigger a graceful shutdown.
    """

    def __init__(self) -> None:
        self.event = trio.Event()

    def trigger(self, *_: Any) -> None:
        """
        Request a graceful shutdown.
        """

        self.event.set()

    async def wait(self) -> None:
        """
        Wait until a graceful shutdown is requested.
        """

        await self.event.wait()

    def connect(self) -> None:  # pragma: no cover
        """
        Listen for termination by the OS in order to trigger a graceful
        shutdown.
        """

        for signal_name in {"SIGINT", "SIGTERM", "SIGBREAK"}:
            if hasattr(signal, signal_name):
                signal.signal(getattr(signal, signal_name), self.trigger)
