"""
This module provides a structured log formatter, using the logfmt format. It
also provides a custom logger for the Hypercorn web server, which logs HTTP
requests in a structured format.
"""

from typing import Any, cast

from http import HTTPStatus
import sys
import traceback

from hypercorn import Config
from hypercorn.logging import Logger as BaseLogger
from hypercorn.typing import HTTPScope, ResponseSummary, WWWScope
from logbook import Handler, Logger, LogRecord, StreamHandler
import logfmt

from ec_rps_config_controller import settings


def logfmt_formatter(record: LogRecord, _handler: Handler) -> str:
    """
    Logbook log formatter.
    """

    logentry = {
        "app": "ec_rps_config_controller",
        "bubble": settings.EC_BUBBLE,
        "time": record.time,
        "level": record.level_name,
        "message": record.message,
    }
    logentry.update(record.extra)

    if record.exc_info:
        exc_type, exc, tb = record.exc_info
        logentry["exc.type"] = f"{exc_type.__name__}"
        logentry["exc.message"] = f"{exc}"
        traceback.print_exception(exc_type, exc, tb, file=sys.stderr)

    logoutput: str = logfmt.format_line(logentry)
    return logoutput


def create_loghandler() -> Handler:
    """
    Logbook handler factory.
    """

    handler = StreamHandler(sys.stdout, level="INFO")
    handler.formatter = logfmt_formatter
    return handler


class WebLogger(BaseLogger):
    """
    Custom logger for the Hypercorn web server.
    """

    def __init__(self, config: Config):
        super().__init__(config)
        self.logger = Logger(__name__)

    async def access(
        self,
        request: WWWScope,
        response: ResponseSummary,
        _request_time: float,
    ) -> None:
        """
        Log an HTTP request.
        """

        if response is None:
            return

        protocol = request.get("http_version", "ws")
        client = request.get("client")

        match client:
            case None:
                remote_addr = None

            case (host, port):
                remote_addr = f"{host}:{port}"

            case (addr,):
                remote_addr = client[0]

            case _:
                remote_addr = f"<???{client}???>"

        match request["type"]:
            case "http":
                method = cast(HTTPScope, request)["method"]

            case _:
                method = "GET"

        qs = request["query_string"].decode()

        status_code = "-"
        status_phrase = "-"

        request_headers = {
            name.decode("latin1").lower(): value.decode("latin1")
            for name, value in request.get("headers", [])
        }
        status_code = str(response["status"])
        try:
            status_phrase = HTTPStatus(response["status"]).phrase

        except ValueError:
            status_phrase = f"<???{status_code}???>"

        response_headers = {
            name.decode("latin1").lower(): value.decode("latin1")
            for name, value in response.get("headers", [])
        }

        extra = {
            "scope": "api",
            "remote_addr": remote_addr,
            "protocol": protocol,
            "method": method,
            "scheme": request["scheme"],
            "path": request["path"],
            "query_string": qs if qs else "",
            "status_code": status_code,
            "status_phrase": status_phrase,
            "content_length": response_headers.get("content-length", "-"),
            "referer": request_headers.get("referer", "-"),
            "user_agent": request_headers.get("user-agent", "-"),
        }

        self.logger.info("HTTP Request", extra=extra)

    async def critical(
        self,
        message: str,
        *args: Any,
        **kwargs: Any,
    ) -> None:
        """
        Log a critical message.
        """

        kwargs["scope"] = "api"
        self.logger.critical(message, *args, extra=kwargs)

    async def error(
        self,
        message: str,
        *args: Any,
        **kwargs: Any,
    ) -> None:
        """
        Log an error message.
        """

        kwargs["scope"] = "api"
        self.logger.error(message, *args, extra=kwargs)

    async def warning(
        self,
        message: str,
        *args: Any,
        **kwargs: Any,
    ) -> None:
        """
        Log a warning message.
        """

        kwargs["scope"] = "api"
        self.logger.warning(message, *args, extra=kwargs)

    async def info(
        self,
        message: str,
        *args: Any,
        **kwargs: Any,
    ) -> None:
        """
        Log an informational message.
        """

        kwargs["scope"] = "api"
        self.logger.info(message, *args, extra=kwargs)

    async def debug(
        self,
        message: str,
        *args: Any,
        **kwargs: Any,
    ) -> None:
        """
        Log a debug message.
        """

        kwargs["scope"] = "api"
        self.logger.debug(message, *args, extra=kwargs)

    async def exception(
        self,
        message: str,
        *args: Any,
        **kwargs: Any,
    ) -> None:
        """
        Log an exception.
        """

        kwargs["scope"] = "api"
        exc_info = kwargs.pop("exc_info", None)
        self.logger.exception(message, *args, extra=kwargs, exc_info=exc_info)
