from typing import cast

from ansible.template import AnsibleEnvironment as Environment
from jinja2 import PackageLoader, Template


def get_template(package: str, path: str) -> Template:
    env = Environment(loader=PackageLoader(package))
    env.trim_blocks = False
    env.lstrip_blocks = False
    return cast(Template, env.get_template(path))
