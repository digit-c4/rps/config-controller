"""
Inspired by Erlang/Elixir `gen_server` design patten, this module provides a
simplistic Actor model.

Based on a decorator `gen_server`, it allows to define a server task that
receives messages from a channel and processes them. Each message contains a
response channel, which is used to send the response back to the caller.
"""

from typing import Generic, cast

from functools import wraps

import trio

from .abc import *


class ChannelWriter(Generic[RequestType, ResponseType]):
    """
    Wrapper around the server's mailbox.
    """

    def __init__(
        self,
        tx: RequestSendChannel[ResponseType, RequestType],
    ) -> None:
        self.tx = tx

    def clone(self) -> "ChannelWriter[RequestType, ResponseType]":
        """
        Create a new channel to send messages to the server.
        """

        return ChannelWriter[RequestType, ResponseType](self.tx.clone())

    async def close(self) -> None:
        """
        Close this channel.
        """

        await self.tx.aclose()

    async def call(self, payload: RequestType) -> ResponseType:
        """
        Send a message to the server and wait for the response.

        If the response is an exception, it is raised.
        """

        resp_tx, resp_rx = trio.open_memory_channel[ResponseMessage[ResponseType]](0)

        msg: RequestMessage[ResponseType, RequestType] = (resp_tx.clone(), payload)
        await self.tx.send(msg)

        resp = await resp_rx.receive()
        await resp_tx.aclose()

        if isinstance(resp, Exception):
            raise resp

        return resp


class gen_server(Generic[RequestType, ResponseType]):
    """
    Decorator that creates the channel for a server task.
    """

    def __call__(
        self,
        task: ServerImplTask[ResponseType, RequestType, ServerParams],
    ) -> ServerWrapperTask[ServerParams]:
        """
        :param task: Server task that processes messages.
        """

        @wraps(task)
        async def server(
            *args: ServerParams.args,
            **kwargs: ServerParams.kwargs,
        ) -> None:
            task_status = cast(
                trio.TaskStatus[ChannelWriter[RequestType, ResponseType]],
                kwargs.pop("task_status", trio.TASK_STATUS_IGNORED),
            )

            chan_tx, chan_rx = trio.open_memory_channel[
                RequestMessage[ResponseType, RequestType]
            ](0)

            async with trio.open_nursery() as nursery:
                await nursery.start(task, chan_rx, *args, **kwargs)
                task_status.started(ChannelWriter[RequestType, ResponseType](chan_tx))

        return server
