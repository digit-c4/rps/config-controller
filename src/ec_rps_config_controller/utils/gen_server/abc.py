from typing import (
    TYPE_CHECKING,
    Awaitable,
    Callable,
    Concatenate,
    ParamSpec,
    TypeAlias,
    TypeVar,
)

from trio import MemoryReceiveChannel, MemorySendChannel, TaskStatus

if TYPE_CHECKING:
    from . import ChannelWriter


RequestType = TypeVar("RequestType", contravariant=True)
ResponseType = TypeVar("ResponseType", covariant=True)

ResponseMessage: TypeAlias = ResponseType | Exception
ResponseSendChannel: TypeAlias = MemorySendChannel[ResponseMessage[ResponseType]]

RequestMessage: TypeAlias = tuple[ResponseSendChannel[ResponseType], RequestType]
RequestSendChannel: TypeAlias = MemorySendChannel[
    RequestMessage[ResponseType, RequestType]
]
RequestReceiveChannel: TypeAlias = MemoryReceiveChannel[
    RequestMessage[ResponseType, RequestType]
]

ServerParams = ParamSpec("ServerParams")

ServerImplTask: TypeAlias = Callable[
    Concatenate[
        RequestReceiveChannel[ResponseType, RequestType],
        ServerParams,
    ],
    Awaitable[None],
]

ServerWrapperTask: TypeAlias = Callable[ServerParams, Awaitable[None]]
