"""
X.509 certificate generation utilities.
"""

from datetime import UTC, datetime, timedelta

from cryptography import x509
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.asymmetric import rsa
import trio


async def self_signed(
    key_path: trio.Path,
    cert_path: trio.Path,
    common_name: str,
) -> None:
    """
    Generates a self-signed X.509 certificate.

    :param key_path: Path to write the private key.
    :param cert_path: Path to write the certificate.
    :param common_name: Common name for the certificate.
    """

    key = rsa.generate_private_key(public_exponent=65537, key_size=2048)
    await key_path.write_bytes(
        key.private_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PrivateFormat.TraditionalOpenSSL,
            encryption_algorithm=serialization.NoEncryption(),
        )
    )

    subject = issuer = x509.Name(
        [
            x509.NameAttribute(x509.NameOID.COMMON_NAME, common_name),
            x509.NameAttribute(x509.NameOID.ORGANIZATION_NAME, "European Commission"),
        ]
    )

    cert = (
        x509.CertificateBuilder()
        .subject_name(subject)
        .issuer_name(issuer)
        .public_key(key.public_key())
        .serial_number(x509.random_serial_number())
        .not_valid_before(datetime.now(UTC))
        .not_valid_after(datetime.now(UTC) + timedelta(days=365))
        .sign(key, hashes.SHA256())
    )

    await cert_path.write_bytes(cert.public_bytes(serialization.Encoding.PEM))


async def check_certificate_pair_match(
    key_path: trio.Path,
    cert_path: trio.Path,
) -> bool:
    """
    Verifies the certificate pair.

    :param key_path: Path to the private key.
    :param cert_path: Path to the certificate (public key).
    :return: True if the public and private keys match.
    """

    key_data = await key_path.read_bytes()
    cert_data = await cert_path.read_bytes()

    try:
        key = serialization.load_pem_private_key(key_data, password=None)
        cert = x509.load_pem_x509_certificate(cert_data)

    except Exception:
        return False

    key_data = key.public_key().public_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PublicFormat.SubjectPublicKeyInfo,
    )
    cert_data = cert.public_key().public_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PublicFormat.SubjectPublicKeyInfo,
    )

    return key_data == cert_data


if __name__ == "__main__":
    import sys

    if len(sys.argv) != 4:
        print(
            f"Usage: {sys.argv[0]} <key_path> <cert_path> <common_name>",
            file=sys.stderr,
        )
        sys.exit(1)

    key_path = trio.Path(sys.argv[1])
    cert_path = trio.Path(sys.argv[2])
    common_name = sys.argv[3]

    try:
        trio.run(self_signed, key_path, cert_path, common_name)

    except Exception as err:
        print(f"Error: {err}", file=sys.stderr)
        sys.exit(1)
