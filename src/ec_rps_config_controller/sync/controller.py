"""
The controller is in charge of managing the lifecycle of mappings. It will
dictate what to do to other components like the State Manager.
"""

from typing import TYPE_CHECKING, Literal, cast

from urllib.parse import urlsplit

import trio

from ec_netbox_client.models import Record
from ec_rps_config_controller.business.mappings import mapping_journalling
from ec_rps_config_controller.utils.gen_server import gen_server

from .abc import (
    ControllerChannelMessage,
    ControllerRequest,
    ControllerResponse,
    StateChannelWriter,
)


@gen_server[ControllerRequest, ControllerResponse]()
async def task(
    request_chan_rx: trio.MemoryReceiveChannel[ControllerChannelMessage],
    state_chan_tx: StateChannelWriter,
    task_status: trio.TaskStatus[None] = trio.TASK_STATUS_IGNORED,
) -> None:
    """
    :param request_chan_rx: Controller's mailbox to receive requests.
    :param state_chan_tx: Channel to communicate with the State Manager.
    """

    await state_chan_tx.call(("write",))
    task_status.started()

    async for respond_to, request in request_chan_rx:
        match request:
            case ("changed", mapping):
                mapping = cast(Record, mapping)

                try:
                    async with mapping_journalling(mapping):
                        common_name = urlsplit(mapping["source"]).hostname
                        if TYPE_CHECKING:
                            assert common_name is not None

                        await state_chan_tx.call(("put", mapping))
                        await state_chan_tx.call(("write",))

                except Exception as err:
                    await respond_to.send(err)

                else:
                    await respond_to.send(None)

            case ("deleted", mapping):
                mapping = cast(Record, mapping)

                try:
                    common_name = urlsplit(mapping["source"]).hostname
                    if TYPE_CHECKING:
                        assert common_name is not None

                    await state_chan_tx.call(("del", mapping["id"]))
                    await state_chan_tx.call(("write",))

                except Exception as err:
                    await respond_to.send(err)

                else:
                    await respond_to.send(None)

            case ("dump", kind):
                kind = cast(Literal["current", "desired"], kind)

                try:
                    resp = await state_chan_tx.call(("dump", kind))
                    if TYPE_CHECKING:
                        assert isinstance(resp, str)

                except Exception as err:
                    await respond_to.send(err)

                else:
                    await respond_to.send(resp)

            case _:
                await respond_to.send(NotImplementedError("Invalid request"))

        await respond_to.aclose()

    await state_chan_tx.close()
