"""
This package is in charge of keeping the Netbox mappings in sync with the
WAF configuration.

It is composed of the following modules:

- ``controller``: The main module that processes the requests from the API
  server and updates the state accordingly.
- ``filewatcher``: A module that watches the certificate files and triggers a
  reload of the WAF configuration when they change.
- ``state_manager``: A module that manages the WAF configuration based on the
  mappings.
"""

import trio

from ec_rps_config_controller import settings

from . import controller, filewatcher, setup, state_manager
from .abc import ControllerChannelWriter, StateChannelWriter


async def task(
    task_status: trio.TaskStatus[ControllerChannelWriter] = trio.TASK_STATUS_IGNORED,
) -> None:
    """
    Main task.
    """

    async with trio.open_nursery() as nursery:
        state_chan_tx: StateChannelWriter = await nursery.start(
            state_manager.task,
        )  # type: ignore[assignment]

        if settings.EC_RPS_CERTS_PATH is not None:
            await nursery.start(filewatcher.task, state_chan_tx.clone())

        request_chan_tx: ControllerChannelWriter = await nursery.start(
            controller.task,
            state_chan_tx,
        )  # type: ignore[assignment]

        await nursery.start(setup.initial_load, request_chan_tx.clone())

        task_status.started(request_chan_tx)
