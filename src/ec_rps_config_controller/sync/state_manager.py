"""
The State Manager is responsible for managing the WAF configuration.
"""

from typing import TYPE_CHECKING, cast

import trio

from ec_netbox_client.models import Record
from ec_rps_config_controller import settings
from ec_rps_config_controller.utils.gen_server import gen_server

from .abc import StateChannelMessage, StateRequest, StateResponse


@gen_server[StateRequest, StateResponse]()
async def task(
    state_chan_rx: trio.MemoryReceiveChannel[StateChannelMessage],
    task_status: trio.TaskStatus[None] = trio.TASK_STATUS_IGNORED,
) -> None:
    """
    :param state_chan_rx: The channel to receive requests from.
    """

    mappings: dict[int, Record] = {}

    task_status.started()

    async for respond_to, request in state_chan_rx:
        match request:
            case ("list",):
                await respond_to.send(list(mappings.values()))

            case ("put", mapping):
                mapping = cast(Record, mapping)

                old_mapping = mappings.get(mapping["id"])
                mappings[mapping["id"]] = mapping
                await respond_to.send(old_mapping)

            case ("get", mapping_id):
                if TYPE_CHECKING:
                    assert isinstance(mapping_id, int)

                await respond_to.send(mappings.get(mapping_id))

            case ("del", mapping_id):
                if TYPE_CHECKING:
                    assert isinstance(mapping_id, int)

                old_mapping = mappings.pop(mapping_id, None)
                await respond_to.send(old_mapping)

            case ("write",):
                try:
                    context = {
                        "env_name": settings.EC_ENV,
                        "restricted_allowed_origins": (
                            None
                            if settings.EC_RPS_ALLOWED_ORIGINS == ["*"]
                            else settings.EC_RPS_ALLOWED_ORIGINS
                        ),
                        "certs_path": settings.EC_RPS_CERTS_PATH,
                        "rps_mappings": mappings.values(),
                    }

                    await settings.EC_RPS_CONFIG_MODULE.render(context)

                except Exception as err:
                    await respond_to.send(err)

                else:
                    await respond_to.send(None)

            case ("dump", "current"):
                try:
                    content = await settings.EC_RPS_CONFIG_MODULE.dump()
                    await respond_to.send(content)

                except Exception as err:
                    await respond_to.send(err)

            case ("dump", "desired"):
                try:
                    context = {
                        "env_name": settings.EC_ENV,
                        "restricted_allowed_origins": (
                            None
                            if settings.EC_RPS_ALLOWED_ORIGINS == ["*"]
                            else settings.EC_RPS_ALLOWED_ORIGINS
                        ),
                        "certs_path": settings.EC_RPS_CERTS_PATH,
                        "rps_mappings": mappings.values(),
                    }
                    content = await settings.EC_RPS_CONFIG_MODULE.render(
                        context,
                        dry_run=True,
                    )
                    await respond_to.send(content)

                except Exception as err:
                    await respond_to.send(err)

            case _:
                await respond_to.send(NotImplementedError("Invalid request"))

        await respond_to.aclose()
