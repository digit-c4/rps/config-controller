"""
This module is responsible for fetching from Netbox the initial mappings.
"""

import logbook
import trio

from ec_rps_config_controller.business.mappings import fetch_mappings

from .abc import ControllerChannelWriter


async def initial_load(
    request_chan_tx: ControllerChannelWriter,
    task_status: trio.TaskStatus[None] = trio.TASK_STATUS_IGNORED,
) -> None:
    """
    Fetch mappings from Netbox on startup.

    :param state_chan_tx: The channel to send requests to the state manager.
    """

    try:
        mappings = await fetch_mappings()
        for mapping in mappings:
            await request_chan_tx.call(("changed", mapping))

    except Exception:
        logbook.exception("Uncaught error")

    await request_chan_tx.close()
    task_status.started()
