from typing import Literal, TypeAlias, Union

import trio

from ec_netbox_client.models import Record
from ec_rps_config_controller.utils.gen_server import ChannelWriter

StateRequest: TypeAlias = Union[
    tuple[Literal["list"]],
    tuple[Literal["put"], Record],
    tuple[Literal["get"], int],
    tuple[Literal["del"], int],
    tuple[Literal["write"]],
    tuple[Literal["dump"], Literal["current", "desired"]],
]
StateResponse: TypeAlias = Union[
    list[Record],
    Record,
    str,
    None,
]
StateChannelMessage: TypeAlias = tuple[
    trio.MemorySendChannel[StateResponse | Exception],
    StateRequest,
]
StateChannelWriter: TypeAlias = ChannelWriter[StateRequest, StateResponse]

ControllerRequest: TypeAlias = Union[
    tuple[Literal["changed"], Record],
    tuple[Literal["deleted"], Record],
    tuple[Literal["dump"], Literal["current", "desired"]],
]
ControllerResponse: TypeAlias = Union[str, None]
ControllerChannelMessage: TypeAlias = tuple[
    trio.MemorySendChannel[ControllerResponse | Exception],
    ControllerRequest,
]
ControllerChannelWriter: TypeAlias = ChannelWriter[
    ControllerRequest, ControllerResponse
]
