"""
Watch certificates and trigger a reload of the WAF configuration when they
change.
"""

from logbook import Logger
from watchdog import events
from watchdog.events import FileSystemEvent, FileSystemEventHandler
from watchdog.observers import Observer
import trio

from ec_rps_config_controller import settings

from .abc import StateChannelWriter

logger = Logger(__name__)


async def task(
    state_chan_tx: StateChannelWriter,
    task_status: trio.TaskStatus[None] = trio.TASK_STATUS_IGNORED,
) -> None:
    """
    :param state_chan_tx: The channel to send requests to the state manager.
    """

    started_event = trio.Event()
    shutdown_event = trio.Event()

    trio_token = trio.lowlevel.current_trio_token()

    try:
        async with trio.open_nursery() as nursery:
            nursery.start_soon(
                trio.to_thread.run_sync,
                thread_worker,
                state_chan_tx,
                started_event,
                shutdown_event,
                trio_token,
            )

            await started_event.wait()
            task_status.started()

    finally:
        shutdown_event.set()
        await state_chan_tx.close()


def thread_worker(
    state_chan_tx: StateChannelWriter,
    started_event: trio.Event,
    shutdown_event: trio.Event,
    trio_token: trio.lowlevel.TrioToken | None = None,
) -> None:
    """
    Thread running the file watcher observer.

    :param state_chan_tx: The channel to send requests to the state manager.
    :param started_event: Event to signal the file watcher has started.
    :param shutdown_event: Event to signal the file watcher should stop.
    """

    assert settings.EC_RPS_CERTS_PATH is not None

    observer = Observer()
    event_handler = NotifyStateManager(state_chan_tx, trio_token=trio_token)
    observer.schedule(
        event_handler,
        settings.EC_RPS_CERTS_PATH.absolute().as_posix(),
        recursive=True,
    )
    observer.start()

    trio.from_thread.run_sync(started_event.set)

    try:
        trio.from_thread.run(shutdown_event.wait)

    finally:
        observer.stop()
        observer.join()


class NotifyStateManager(FileSystemEventHandler):
    """
    FileSystem event handler that notifies the state manager when a file change.
    """

    def __init__(
        self,
        state_chan_tx: StateChannelWriter,
        trio_token: trio.lowlevel.TrioToken | None = None,
    ):
        """
        :param state_chan_tx: The channel to send requests to the state manager.
        """

        super().__init__()
        self.state_chan_tx = state_chan_tx
        self.trio_token = trio_token

    def on_any_event(self, event: FileSystemEvent) -> None:
        """
        Event handling implementation.
        """

        listen = [
            events.EVENT_TYPE_CREATED,
            events.EVENT_TYPE_DELETED,
            events.EVENT_TYPE_MODIFIED,
            events.EVENT_TYPE_MOVED,
        ]

        if event.event_type in listen:
            extra = extra = {
                "file.event": event.event_type,
                "file.src_path": event.src_path,
                "file.dest_path": event.dest_path,
                "file.is_directory": event.is_directory,
            }

            logger.info("detected file change", extra=extra)

            try:
                trio.from_thread.run(
                    self.state_chan_tx.call,
                    ("write",),
                    trio_token=self.trio_token,
                )

            except Exception as err:
                logger.exception(f"{err}", extra=extra)
