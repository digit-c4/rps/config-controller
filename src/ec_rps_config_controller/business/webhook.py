"""
This module contains the business logic for the Config Controller, related to
the Netbox webhooks.
"""

from typing import TYPE_CHECKING, cast

from socket import gethostname

from logbook import Logger
from tenacity import retry, stop_after_attempt, wait_random_exponential
import trio

from ec_netbox_client import ECNetboxClient
from ec_netbox_client.operations import (
    extras_webhooks_bulk_destroy,
    extras_webhooks_create,
    extras_webhooks_list,
)
from ec_rps_config_controller import settings

logger = Logger(__name__)


@retry(
    reraise=True,
    stop=stop_after_attempt(5),
    wait=wait_random_exponential(
        multiplier=settings.RETRY_WAIT_MULTIPLIER,
        max=settings.RETRY_WAIT_MAX,
    ),
    sleep=trio.sleep,
)
async def setup_webhook() -> None:
    """
    Setup a webhook to receive updates from Netbox.
    """

    client = ECNetboxClient(
        base_url=settings.NETBOX_API,
        token=settings.NETBOX_TOKEN,
        verify_ssl=False,
        raise_on_unexpected_status=True,
        logger=logger,
    )
    hostname = gethostname()
    webhook_name = f"{hostname}/{settings.EC_RPS_WEBHOOK_SCOPE}"
    webhook_url = f"http://{hostname}/snet/agent/webhook/netbox/"
    apikey = settings.EC_RPS_SECRET

    res = await extras_webhooks_list(
        client=client,
        params=dict(
            name=[webhook_name],
        ),
    )

    if TYPE_CHECKING:
        assert res is not None, "should raise on unexpected status"
        assert isinstance(res["results"], list), "never unset when in response"

    await extras_webhooks_bulk_destroy(
        client=client,
        body=res["results"],
    )

    await extras_webhooks_create(
        client=client,
        body=dict(
            name=webhook_name,
            content_types=["netbox_rps_plugin.mapping"],
            enabled=True,
            type_create=True,
            type_update=True,
            type_delete=True,
            payload_url=webhook_url,
            http_method="POST",
            http_content_type="application/json",
            secret=apikey,
            conditions={
                "and": [
                    {
                        "attr": "tags.slug",
                        "op": "contains",
                        "value": settings.EC_ENV,
                    },
                ],
            },
            ssl_verification=False,
        ),
    )
