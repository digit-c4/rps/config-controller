"""
This module contains the business logic for the Config Controller, related to
RPS mappings objects in Netbox.
"""

from typing import TYPE_CHECKING, AsyncGenerator

from contextlib import asynccontextmanager

from logbook import Logger
from tenacity import retry, stop_after_attempt, wait_random_exponential
import trio

from ec_netbox_client import ECNetboxClient
from ec_netbox_client.models import Record
from ec_netbox_client.operations import (
    extras_journal_entries_create,
    plugins_rps_mapping_list,
)
from ec_rps_config_controller import settings

logger = Logger(__name__)


@retry(
    reraise=True,
    stop=stop_after_attempt(5),
    wait=wait_random_exponential(
        multiplier=settings.RETRY_WAIT_MULTIPLIER,
        max=settings.RETRY_WAIT_MAX,
    ),
    sleep=trio.sleep,
)
async def fetch_mappings() -> list[Record]:
    """
    Fetch mappings from the Netbox API.
    """

    client = ECNetboxClient(
        base_url=settings.NETBOX_API,
        token=settings.NETBOX_TOKEN,
        verify_ssl=False,
        raise_on_unexpected_status=True,
        logger=logger,
    )

    res = await plugins_rps_mapping_list(
        client=client,
        params=dict(
            tag=[settings.EC_ENV],
        ),
    )

    if TYPE_CHECKING:
        assert res is not None, "should raise on unexpected status"
        assert isinstance(res["results"], list), "never unset when in response"

    return res["results"]


@retry(
    retry_error_callback=lambda *_: None,
    stop=stop_after_attempt(5),
    wait=wait_random_exponential(
        multiplier=settings.RETRY_WAIT_MULTIPLIER,
        max=settings.RETRY_WAIT_MAX,
    ),
    sleep=trio.sleep,
)
async def create_mapping_journal_entry(
    mapping: Record,
    kind: str,
    comment: str,
) -> None:
    """
    Create a journal entry for a mapping in the Netbox API.
    """

    client = ECNetboxClient(
        base_url=settings.NETBOX_API,
        token=settings.NETBOX_TOKEN,
        verify_ssl=False,
        raise_on_unexpected_status=True,
        logger=logger,
    )

    await extras_journal_entries_create(
        client=client,
        body=dict(
            assigned_object_id=mapping["id"],
            assigned_object_type="netbox_rps_plugin.mapping",
            kind=kind,
            comments=comment,
        ),
    )


@asynccontextmanager
async def mapping_journalling(
    mapping: Record,
    phase: str = "deployment",
) -> AsyncGenerator[None, None]:
    """
    Wrapper for a block of code that deploys a mapping, logging via journal
    entries the progress of the deployment.
    """

    await create_mapping_journal_entry(
        mapping,
        "info",
        f"{phase.capitalize()} in progress",
    )

    try:
        yield

    except Exception as err:
        await create_mapping_journal_entry(
            mapping,
            "danger",
            f"{phase.capitalize()} failed: {err}",
        )
        raise

    else:
        await create_mapping_journal_entry(
            mapping,
            "success",
            f"{phase.capitalize()} successful",
        )
