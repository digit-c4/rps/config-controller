"""
This module contains the API server for the Config Controller. It is responsible
for receiving updates from the Netbox webhook and forwarding them to the
controller.
"""

from typing import Any, Awaitable, Callable, cast

import hashlib
import hmac

from fastapi import Depends, FastAPI, HTTPException, Request
from hypercorn.config import Config
from hypercorn.trio import serve
from hypercorn.typing import ASGIFramework
import trio

from ec_rps_config_controller import settings
from ec_rps_config_controller.logging import WebLogger
from ec_rps_config_controller.sync.abc import ControllerChannelWriter

from .router import router


async def verify_request_signature(request: Request) -> None:
    """
    Netbox hashes the request body with the secret key using HMAC-SHA512 and
    sends the hash in the X-Hook-Signature header. This function verifies the
    signature to ensure the request is authentic.

    :raises HTTPException:
        If the signature is invalid, a 403 exception is raised.
    """

    content = await request.body()
    digest = hmac.new(
        key=settings.EC_RPS_SECRET.encode(),
        msg=content,
        digestmod=hashlib.sha512,
    ).hexdigest()

    signature = request.headers.get("X-Hook-Signature")
    if signature != digest:
        raise HTTPException(status_code=403, detail="Invalid request signature")


async def _serve(
    app: ASGIFramework,
    config: Config,
    shutdown_trigger: Callable[..., Awaitable[None]],
    task_status: trio.TaskStatus[list[str]] = trio.TASK_STATUS_IGNORED,
) -> None:
    await serve(
        app,
        config,
        shutdown_trigger=shutdown_trigger,
        task_status=cast(Any, task_status),
    )


async def asgi_app(
    chan_tx: ControllerChannelWriter,
    shutdown_trigger: Callable[..., Awaitable[None]],
    task_status: trio.TaskStatus[list[str]] = trio.TASK_STATUS_IGNORED,
) -> None:
    """
    ASGI Server task.

    :param chan_tx: The channel to send requests to the controller.
    :param shutdown_trigger: The trigger for graceful shutdown.
    """

    cfg = Config()
    cfg.logger_class = WebLogger
    cfg.bind = [f"127.0.0.1:{settings.BIND_PORT}"]

    app = FastAPI(
        title="EC WAF Agent API",
        version="0.1.0",
        dependencies=[Depends(verify_request_signature)],
    )
    app.state.get_sync_channel = lambda: chan_tx.clone()
    app.include_router(router)

    async with trio.open_nursery() as nursery:
        binds: list[str] = await nursery.start(
            _serve,
            cast(ASGIFramework, app),
            cfg,
            shutdown_trigger,
        )  # type: ignore[assignment]

        task_status.started(binds)

    await chan_tx.close()
