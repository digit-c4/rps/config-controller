"""
This module provides the API routes for the Config Controller.
"""

from typing import TYPE_CHECKING, Literal, Optional

from fastapi import APIRouter, HTTPException, Request, Response
from logbook import Logger
from pydantic import BaseModel

from ec_netbox_client.models import Record
from ec_rps_config_controller.sync.abc import ControllerChannelWriter


class WebhookPayloadSnapshots(BaseModel):
    prechange: Optional[Record]
    postchange: Optional[Record]


class WebhookPayload(BaseModel):
    event: str
    timestamp: str
    model: str
    username: str
    request_id: str
    data: Record
    snapshots: WebhookPayloadSnapshots


class WebhookResponse(BaseModel):
    success: bool


class HealthcheckResponse(BaseModel):
    status: Literal["ok", "error"]
    detail: Optional[str]


router = APIRouter()
logger = Logger(__name__)


@router.get("/health")
async def healthcheck() -> HealthcheckResponse:
    """
    Always returns 200 OK.
    """

    return HealthcheckResponse(status="ok", detail=None)


@router.get("/debug/config/current")
async def debug_config_current(request: Request) -> Response:
    """
    Returns the currently generated configuration.
    """

    try:
        request_chan_tx: ControllerChannelWriter = request.app.state.get_sync_channel()
        content = await request_chan_tx.call(("dump", "current"))
        if TYPE_CHECKING:
            assert isinstance(content, str)

        return Response(content, media_type="text/plain")

    except Exception as err:
        logger.exception(f"{err}")
        raise HTTPException(status_code=500, detail=f"{err}")

    finally:
        await request_chan_tx.close()


@router.get("/debug/config/desired")
async def debug_config_desired(request: Request) -> Response:
    """
    Returns the desired configuration (could be invalid).
    """

    try:
        request_chan_tx: ControllerChannelWriter = request.app.state.get_sync_channel()
        content = await request_chan_tx.call(("dump", "desired"))
        if TYPE_CHECKING:
            assert isinstance(content, str)

        return Response(content, media_type="text/plain")

    except Exception as err:
        logger.exception(f"{err}")
        raise HTTPException(status_code=500, detail=f"{err}")

    finally:
        await request_chan_tx.close()


@router.post("/webhook/netbox")
async def webhook_netbox(
    request: Request,
    payload: WebhookPayload,
) -> WebhookResponse:
    """
    Endpoint for receiving Netbox webhooks.
    """

    if payload.model == "mapping":
        mapping = payload.data

        request_chan_tx: ControllerChannelWriter = request.app.state.get_sync_channel()

        try:
            match payload.event:
                case "created" | "updated":
                    await request_chan_tx.call(("changed", mapping))

                case "deleted":
                    await request_chan_tx.call(("deleted", mapping))

                case _:
                    raise NotImplementedError("Invalid event")

        except NotImplementedError as err:
            raise HTTPException(status_code=400, detail=f"{err}")

        except Exception as err:
            logger.exception(f"{err}")
            raise HTTPException(status_code=500, detail=f"{err}")

        finally:
            await request_chan_tx.close()

    return WebhookResponse(success=True)
