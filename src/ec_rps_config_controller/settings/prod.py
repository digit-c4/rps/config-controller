from importlib import import_module
from pathlib import Path
import secrets

from decouple import config

DEBUG: bool = config("DEBUG", default=False, cast=bool)
BIND_PORT: int = config("PORT", default=8000, cast=int)

NETBOX_API: str = config("NETBOX_API")
NETBOX_TOKEN: str = config("NETBOX_TOKEN")

EC_RPS_SECRET: str = secrets.token_hex(32)
EC_RPS_ALLOWED_ORIGINS: list[str] = config(
    "EC_RPS_ALLOWED_ORIGINS",
    default="*",
    cast=lambda val: [origin.strip() for origin in val.split(",")],
)
EC_RPS_CERTS_PATH: Path | None = config(
    "EC_RPS_CERTS_PATH",
    default=None,
    cast=lambda path: None if not path else Path(path),
)
EC_RPS_WEBHOOK_SCOPE: str = config("EC_RPS_WEBHOOK_SCOPE")

EC_BUBBLE: str = config("EC_BUBBLE")
EC_ENV: str = config("EC_ENV", default=EC_BUBBLE)

EC_RPS_CONFIG_MODULE_PATH: str = config("EC_RPS_CONFIG_MODULE")
EC_RPS_CONFIG_MODULE = import_module(EC_RPS_CONFIG_MODULE_PATH)

RETRY_WAIT_MULTIPLIER = 1
RETRY_WAIT_MAX = 10
