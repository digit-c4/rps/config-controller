from typing import TYPE_CHECKING

import sys

from decouple import config

_settings_module: str = config(
    "EC_RPS_SETTINGS_MODULE",
    default="ec_rps_config_controller.settings.prod",
)

try:
    __import__(_settings_module)

except ImportError as err:
    print(err, file=sys.stderr)
    sys.exit(1)

else:
    _mod = sys.modules[_settings_module]
    locals().update(_mod.__dict__)

if TYPE_CHECKING:
    from typing import Any, Protocol

    from pathlib import Path

    class RenderModule(Protocol):
        async def render(
            self, context: dict[str, Any], dry_run: bool = False
        ) -> str: ...
        async def dump(self) -> str: ...

    DEBUG: bool
    BIND_PORT: int

    NETBOX_API: str
    NETBOX_TOKEN: str

    EC_RPS_SECRET: str
    EC_RPS_ALLOWED_ORIGINS: list[str]
    EC_RPS_CERTS_PATH: Path | None
    EC_RPS_WEBHOOK_SCOPE: str

    EC_BUBBLE: str
    EC_ENV: str

    EC_RPS_CONFIG_MODULE_PATH: str
    EC_RPS_CONFIG_MODULE: RenderModule

    RETRY_WAIT_MULTIPLIER: int | float
    RETRY_WAIT_MAX: int | float
