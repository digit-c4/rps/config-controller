from importlib import import_module
from pathlib import Path

DEBUG: bool = True
BIND_PORT: int = 0

NETBOX_API: str = "http://localhost"
NETBOX_TOKEN: str = "s3cr3t"

EC_RPS_SECRET: str = "s3cr3t"
EC_RPS_ALLOWED_ORIGINS: list[str] = ["*"]
EC_RPS_CERTS_PATH: Path | None = Path("/etc/ssl/certs")
EC_RPS_WEBHOOK_SCOPE: str = "test"

EC_BUBBLE: str = "test"
EC_ENV: str = "test"

EC_RPS_CONFIG_MODULE_PATH: str = "tests.mocks.config_dummy"
EC_RPS_CONFIG_MODULE = import_module(EC_RPS_CONFIG_MODULE_PATH)

RETRY_WAIT_MULTIPLIER = 0.01
RETRY_WAIT_MAX = 0.1
