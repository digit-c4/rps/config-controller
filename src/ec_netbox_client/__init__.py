from typing import Any, cast

from httpx import AsyncClient, Request, Response
from logbook import Logger


class ECNetboxClient:
    def __init__(
        self,
        base_url: str,
        token: str,
        verify_ssl: bool,
        raise_on_unexpected_status: bool,
        logger: Logger,
    ):
        self.raise_on_unexpected_status = raise_on_unexpected_status
        self.logger = logger

        self.client = AsyncClient(
            base_url=base_url,
            headers={"Authorization": f"Token {token}"},
            verify=verify_ssl,
            event_hooks={
                "request": [self._log_request],
                "response": [Response.aread, self._log_response],
            },
        )

    async def operation(
        self,
        method: str,
        path: str,
        params: dict[str, Any] | None = None,
        body: dict[str, Any] | list[dict[str, Any]] | None = None,
    ) -> dict[str, Any] | None:
        response = await self.client.request(
            method,
            path,
            params=params,
            json=body,
        )

        if self.raise_on_unexpected_status:
            response.raise_for_status()

        if response.status_code == 204:
            return None

        return cast(dict[str, Any], response.json())

    async def _log_request(self, request: Request) -> None:
        self.logger.info(
            "netbox request",
            extra={
                "request.method": request.method,
                "request.url": request.url,
            },
        )

    async def _log_response(self, response: Response) -> None:
        extra = {
            "request.method": response.request.method,
            "request.url": response.request.url,
            "response.status_code": response.status_code,
            "response.reason_phrase": response.reason_phrase,
        }

        if response.is_error:
            log = self.logger.error
            extra["response.body"] = response.text

        else:
            log = self.logger.info

        log("netbox response", extra=extra)
