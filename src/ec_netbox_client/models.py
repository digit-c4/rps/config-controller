from typing import Any, TypedDict

type Record = dict[str, Any]


class RecordList(TypedDict):
    count: int
    next: str | None
    previous: str | None
    results: list[Record]
