from typing import Any, cast

from ec_netbox_client import ECNetboxClient
from ec_netbox_client.models import RecordList


async def plugins_rps_mapping_list(
    client: ECNetboxClient,
    params: dict[str, Any],
) -> RecordList:
    result = await client.operation(
        "GET",
        "/api/plugins/rps/mapping/",
        params=params,
    )
    return cast(RecordList, result)
