from typing import Any, cast

from ec_netbox_client import ECNetboxClient
from ec_netbox_client.models import Record, RecordList


async def extras_journal_entries_create(
    client: ECNetboxClient,
    body: dict[str, Any],
) -> Record:
    result = await client.operation(
        "POST",
        "/api/extras/journal-entries/",
        body=body,
    )
    return cast(Record, result)


async def extras_webhooks_list(
    client: ECNetboxClient,
    params: dict[str, Any],
) -> RecordList:
    result = await client.operation(
        "GET",
        "/api/extras/webhooks/",
        params=params,
    )
    return cast(RecordList, result)


async def extras_webhooks_bulk_destroy(
    client: ECNetboxClient,
    body: list[dict[str, Any]],
) -> None:
    await client.operation(
        "DELETE",
        "/api/extras/webhooks/",
        body=body,
    )


async def extras_webhooks_create(
    client: ECNetboxClient,
    body: dict[str, Any],
) -> Record:
    result = await client.operation(
        "POST",
        "/api/extras/webhooks/",
        body=body,
    )
    return cast(Record, result)
