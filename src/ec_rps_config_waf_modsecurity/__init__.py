from typing import Any

import trio

from ec_rps_config_controller.utils.templates import get_template

from . import settings, utils


async def render(context: dict[str, Any], dry_run: bool = False) -> str:
    context["acme_enabled"] = settings.EC_RPS_ACME_ENABLED
    context["proxy_host"] = settings.EC_RPS_PROXY_HOST
    context["proxy_protocol"] = settings.EC_RPS_PROXY_PROTOCOL
    context["upstream_has_tls"] = settings.EC_RPS_UPSTREAM_HAS_TLS

    template = get_template("ec_rps_config_waf_modsecurity", "apache2.j2")
    content = template.render(context)

    if not dry_run:
        async with trio.open_nursery() as nursery:
            for mapping in context["rps_mappings"]:
                nursery.start_soon(
                    utils.ensure_cert,
                    mapping["source"],
                )

        await utils.generate_config(content)

    return content


async def dump() -> str:
    return await utils.CONFIG_PATH.read_text()
