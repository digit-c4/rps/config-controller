from decouple import config

EC_RPS_ACME_ENABLED: str = config("EC_RPS_ACME_ENABLED", default=True, cast=bool)
EC_RPS_PROXY_HOST: str = config("EC_RPS_PROXY_HOST")

# Legacy compatibility
EC_RPS_PROXY_PROTOCOL: bool = config(
    "EC_RPS_PROXY_PROTOCOL",
    default=True,
    cast=bool,
)

EC_RPS_UPSTREAM_HAS_TLS: bool = config(
    "EC_RPS_UPSTREAM_HAS_TLS",
    default=False,
    cast=bool,
)
