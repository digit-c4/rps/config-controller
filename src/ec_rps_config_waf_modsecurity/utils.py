"""
This module provides the utility functions to manage modsec-based WAF
configuration.
"""

from urllib.parse import urlsplit

import trio

from ec_rps_config_controller import settings
from ec_rps_config_controller.utils import x509

from . import settings as waf_modsec_settings

CONFIG_PATH = trio.Path("/etc/apache2/conf.d/99-mappings.conf")
BACKUP_PATH = trio.Path("/etc/apache2/conf.d/99-mappings.conf.bak")


async def ensure_cert(url: str) -> None:
    """
    Generates a placeholder self-signed certificate for the given URL if no
    certificate exist, otherwise verify its public key and private key match.
    """

    hostname = urlsplit(url).hostname
    if hostname is None:
        raise ValueError("Invalid mapping URL, missing hostname")

    yes, cert_paths = await should_create_cert(hostname)
    key_path, cert_path = cert_paths

    if yes:
        await x509.self_signed(key_path, cert_path, hostname)

    matched = await x509.check_certificate_pair_match(key_path, cert_path)
    if not matched:
        raise RuntimeError("Certificate pair mismatch")


async def should_create_cert(
    hostname: str,
) -> tuple[bool, tuple[trio.Path, trio.Path]]:  # pragma: no cover
    """
    Determines if a certificate should be created for the given hostname.
    """

    folder = "acme" if waf_modsec_settings.EC_RPS_ACME_ENABLED else "static"
    key_path = trio.Path(f"{settings.EC_RPS_CERTS_PATH}/{folder}/{hostname}.key")
    cert_path = trio.Path(f"{settings.EC_RPS_CERTS_PATH}/{folder}/{hostname}.pub")

    if not await key_path.exists() or not await cert_path.exists():
        return True, (key_path, cert_path)

    return False, (key_path, cert_path)


async def generate_config(content: str) -> None:
    """
    Backups the configuration, generate the new one, validates it, and reloads
    the webserver, or rollback to the previous valid configuration.
    """

    await backup_config()
    await write_config(content)

    try:
        await check_config()

    except Exception:
        await rollback_config()
        raise

    await reload_webserver()


async def backup_config() -> None:  # pragma: no cover
    """
    Backups the configuration file.
    """

    if await CONFIG_PATH.exists():
        await CONFIG_PATH.rename(BACKUP_PATH)


async def write_config(content: str) -> None:  # pragma: no cover
    """
    Writes the configuration file.
    """

    await CONFIG_PATH.write_text(content)


async def rollback_config() -> None:  # pragma: no cover
    """
    Rollbacks the configuration file to the previous valid one.
    """

    await CONFIG_PATH.unlink()

    if await BACKUP_PATH.exists():
        await BACKUP_PATH.rename(CONFIG_PATH)


async def check_config() -> None:  # pragma: no cover
    """
    Check for syntax errors in the configuration file.
    """

    res = await trio.run_process(
        "apachectl -t",
        shell=True,
        check=False,
        capture_stdout=True,
        capture_stderr=True,
    )

    if res.returncode != 0:
        raise RuntimeError(res.stderr.decode())


async def reload_webserver() -> None:  # pragma: no cover
    """
    Reloads webserver.
    """

    res = await trio.run_process(
        "apachectl -k graceful",
        shell=True,
        check=False,
        capture_stdout=True,
        capture_stderr=True,
    )

    if res.returncode != 0:
        raise RuntimeError(res.stderr.decode())
