from decouple import config

EC_RPS_NGINX_PLUS_ENABLED: bool = config(
    "EC_RPS_NGINX_PLUS_ENABLED",
    default=False,
    cast=bool,
)

EC_RPS_MAX_CACHE_SIZE: str = config("EC_RPS_MAX_CACHE_SIZE", default="2g")
EC_RPS_TRUSTED_IPS: list[str] = config(
    "EC_RPS_TRUSTED_IPS",
    default="",
    cast=lambda val: [] if not val else [ip.strip() for ip in val.split(",")],
)

EC_RPS_SAML_ENABLED: bool = config("EC_RPS_SAML_ENABLED", default=True, cast=bool)

if EC_RPS_SAML_ENABLED:
    EC_RPS_SAML_SP_ENTITY_ID: str = config("EC_RPS_SAML_SP_ENTITY_ID")
    EC_RPS_SAML_SP_ACS_URL: str = config("EC_RPS_SAML_SP_ACS_URL")
    EC_RPS_SAML_SP_REQUEST_BINDING: str = config(
        "EC_RPS_SAML_SP_REQUEST_BINDING",
        default="HTTP-POST",
    )
    EC_RPS_SAML_SP_SIGN_AUTHN: bool = config(
        "EC_RPS_SAML_SP_SIGN_AUTHN",
        default=False,
        cast=bool,
    )
    EC_RPS_SAML_SP_SIGNING_KEY: str = config(
        "EC_RPS_SAML_SP_SIGNING_KEY",
        default="",
    )
    EC_RPS_SAML_SP_DECRYPTION_KEY: str = config(
        "EC_RPS_SAML_SP_DECRYPTION_KEY",
        default="",
    )
    EC_RPS_SAML_SP_FORCE_AUTHN: bool = config(
        "EC_RPS_SAML_SP_FORCE_AUTHN",
        default=False,
        cast=bool,
    )
    EC_RPS_SAML_SP_NAMEID_FORMAT: str = config(
        "EC_RPS_SAML_SP_NAMEID_FORMAT",
        default="urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified",
    )
    EC_RPS_SAML_SP_RELAY_STATE: str = config(
        "EC_RPS_SAML_SP_RELAY_STATE",
        default="",
    )
    EC_RPS_SAML_SP_WANT_SIGNED_RESPONSE: bool = config(
        "EC_RPS_SAML_SP_WANT_SIGNED_RESPONSE",
        default=False,
        cast=bool,
    )
    EC_RPS_SAML_SP_WANT_SIGNED_ASSERTION: bool = config(
        "EC_RPS_SAML_SP_WANT_SIGNED_ASSERTION",
        default=False,
        cast=bool,
    )
    EC_RPS_SAML_SP_WANT_ENCRYPTED_ASSERTION: bool = config(
        "EC_RPS_SAML_SP_WANT_ENCRYPTED_ASSERTION",
        default=False,
        cast=bool,
    )

    EC_RPS_SAML_SP_SLO_URL: str = config("EC_RPS_SAML_SP_SLO_URL")
    EC_RPS_SAML_SP_SLO_BINDING: str = config(
        "EC_RPS_SAML_SP_SLO_BINDING",
        default="HTTP-POST",
    )

    EC_RPS_SAML_SP_SIGN_SLO: bool = config(
        "EC_RPS_SAML_SP_SIGN_SLO",
        default=False,
        cast=bool,
    )
    EC_RPS_SAML_SP_WANT_SIGNED_SLO: bool = config(
        "EC_RPS_SAML_SP_WANT_SIGNED_SLO",
        default=False,
        cast=bool,
    )

    EC_RPS_SAML_IDP_ENTITY_ID: str = config("EC_RPS_SAML_IDP_ENTITY_ID")
    EC_RPS_SAML_IDP_SSO_URL: str = config("EC_RPS_SAML_IDP_SSO_URL")
    EC_RPS_SAML_IDP_VERIFICATION_CERTIFICATE: str = config(
        "EC_RPS_SAML_IDP_VERIFICATION_CERTIFICATE",
        default="",
    )

    EC_RPS_SAML_IDP_SLO_URL: str = config("EC_RPS_SAML_IDP_SLO_URL")

    EC_RPS_SAML_SP_REDIRECT_BASE: str = config("EC_RPS_SAML_SP_REDIRECT_BASE")

    EC_RPS_SAML_AUTH_HEADER_NAME: str = config(
        "EC_RPS_SAML_AUTH_HEADER_NAME",
        default="Remote-User",
    )
