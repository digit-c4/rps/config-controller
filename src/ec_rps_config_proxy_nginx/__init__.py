from typing import Any

from logbook import Logger

from ec_rps_config_controller.utils.templates import get_template

from . import settings, utils

logger = Logger(__name__)


async def render(context: dict[str, Any], dry_run: bool = False) -> str:
    context["nginx_plus_enabled"] = settings.EC_RPS_NGINX_PLUS_ENABLED
    context["max_cache_size"] = settings.EC_RPS_MAX_CACHE_SIZE
    context["trusted_ips"] = settings.EC_RPS_TRUSTED_IPS
    context["saml_enabled"] = settings.EC_RPS_SAML_ENABLED

    if settings.EC_RPS_SAML_ENABLED:
        if not settings.EC_RPS_NGINX_PLUS_ENABLED:
            logger.warning("SAML is enabled but NGINX Plus is not enabled")

        context["saml_config"] = {
            "sp_entity_id": settings.EC_RPS_SAML_SP_ENTITY_ID,
            "sp_acs_url": settings.EC_RPS_SAML_SP_ACS_URL,
            "sp_request_binding": settings.EC_RPS_SAML_SP_REQUEST_BINDING,
            "sp_sign_authn": settings.EC_RPS_SAML_SP_SIGN_AUTHN,
            "sp_signing_key": settings.EC_RPS_SAML_SP_SIGNING_KEY,
            "sp_decryption_key": settings.EC_RPS_SAML_SP_DECRYPTION_KEY,
            "sp_force_authn": settings.EC_RPS_SAML_SP_FORCE_AUTHN,
            "sp_nameid_format": settings.EC_RPS_SAML_SP_NAMEID_FORMAT,
            "sp_relay_state": settings.EC_RPS_SAML_SP_RELAY_STATE,
            "sp_want_signed_response": settings.EC_RPS_SAML_SP_WANT_SIGNED_RESPONSE,
            "sp_want_signed_assertion": settings.EC_RPS_SAML_SP_WANT_SIGNED_ASSERTION,
            "sp_want_encrypted_assertion": settings.EC_RPS_SAML_SP_WANT_ENCRYPTED_ASSERTION,
            "sp_slo_url": settings.EC_RPS_SAML_SP_SLO_URL,
            "sp_slo_binding": settings.EC_RPS_SAML_SP_SLO_BINDING,
            "sp_sign_slo": settings.EC_RPS_SAML_SP_SIGN_SLO,
            "sp_want_signed_slo": settings.EC_RPS_SAML_SP_WANT_SIGNED_SLO,
            "idp_entity_id": settings.EC_RPS_SAML_IDP_ENTITY_ID,
            "idp_sso_url": settings.EC_RPS_SAML_IDP_SSO_URL,
            "idp_verification_certificate": settings.EC_RPS_SAML_IDP_VERIFICATION_CERTIFICATE,
            "idp_slo_url": settings.EC_RPS_SAML_IDP_SLO_URL,
            "sp_redirect_base": settings.EC_RPS_SAML_SP_REDIRECT_BASE,
            "auth_header_name": settings.EC_RPS_SAML_AUTH_HEADER_NAME,
        }

    template = get_template("ec_rps_config_proxy_nginx", "nginx.j2")
    content = template.render(context)

    if not dry_run:
        await utils.generate_config(content)

    return content


async def dump() -> str:
    return await utils.NGINX_CONFIG_PATH.read_text()
