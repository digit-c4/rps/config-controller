"""
This module provides the utility functions to manage NGINX-based proxy
configuration.
"""

import trio

NGINX_CONFIG_PATH = trio.Path("/etc/nginx/conf.d/default.conf")
NGINX_BACKUP_PATH = trio.Path("/etc/nginx/conf.d/default.conf.bak")


async def generate_config(content: str) -> None:
    """
    Backups the NGINX+ configuration, generate the new one, validates it, and
    reloads NGINX+, or rollback to the previous valid configuration.
    """

    await backup_config()
    await write_config(content)

    try:
        await nginx_check()
        await gixy_check()

    except Exception:
        await rollback_config()
        raise

    await nginx_reload()


async def backup_config() -> None:  # pragma: no cover
    """
    Backups the NGINX+ configuration file.
    """

    if await NGINX_CONFIG_PATH.exists():
        await NGINX_CONFIG_PATH.rename(NGINX_BACKUP_PATH)


async def write_config(content: str) -> None:  # pragma: no cover
    """
    Writes the NGINX+ configuration file.
    """

    await NGINX_CONFIG_PATH.write_text(content)


async def rollback_config() -> None:  # pragma: no cover
    """
    Rollbacks the NGINX+ configuration file to the previous valid one.
    """

    await NGINX_CONFIG_PATH.unlink()

    if await NGINX_BACKUP_PATH.exists():
        await NGINX_BACKUP_PATH.rename(NGINX_CONFIG_PATH)


async def nginx_check() -> None:  # pragma: no cover
    """
    Check for syntax errors in the NGINX+ configuration file.
    """

    res = await trio.run_process(
        "nginx -t",
        shell=True,
        check=False,
        capture_stdout=True,
        capture_stderr=True,
    )

    if res.returncode != 0:
        raise RuntimeError(res.stderr.decode())


async def gixy_check() -> None:  # pragma: no cover
    """
    Check for security issues in the NGINX+ configuration file.
    """

    res = await trio.run_process(
        "gixy",
        shell=True,
        check=False,
        stdin=None,
        capture_stdout=True,
        capture_stderr=True,
    )

    if res.returncode != 0:
        raise RuntimeError(res.stdout.decode())


async def nginx_reload() -> None:  # pragma: no cover
    """
    Reloads NGINX+.
    """

    await trio.run_process(
        "nginx -s reload",
        shell=True,
        check=True,
        capture_stdout=True,
        capture_stderr=True,
    )
