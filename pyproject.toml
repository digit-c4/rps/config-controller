[tool.poetry]
name = "ec-rps-config-controller"
version = "0.25.0"
description = "Generate configuration from Netbox RPS Mappings"
authors = ["European Commission"]
readme = "README.md"

[tool.poetry.dependencies]
python = "^3.12"
python-decouple = "^3.8"
trio = "^0.29.0"
tenacity = "^9.0.0"
watchdog = "^6.0.0"
jinja2 = "^3.1.6"
fastapi = "^0.115.11"
hypercorn = "^0.17.3"
logbook = "^1.8.0"
logfmt = "^0.4"
ansible = "^11.3.0"
gixy-ng = "^0.2.7"
httpx = "^0.28.1"

[tool.poetry.group.dev.dependencies]
isort = "^6.0.1"
black = "^25.1.0"
mypy = "^1.15.0"
pytest = "^8.3.5"
pytest-env = "^1.1.5"
pytest-trio = "^0.8.0"
pytest-cov = "^6.0.0"
freezegun = "^1.5.1"
httpx = "^0.28.1"

[tool.poetry.group.doc.dependencies]
sphinx = "^8.2.3"
autodoc-pydantic = "^2.2.0"
sphinx-typlog-theme = {git = "https://code.europa.eu/delasda/sphinx-typlog-theme"}
diagrams = "^0.24.4"
sphinx-diagrams = "^0.4.0"
marimo = "^0.11.17"
lxml = "^5.3.1"

[tool.isort]
known_future_library = ["__future__", "typing"]
known_first_party = [
  "ec_rps_config_controller",
  "ec_rps_config_proxy_nginx",
  "ec_rps_config_waf_coraza",
  "ec_rps_config_waf_modsecurity",
  "ec_netbox_client",
]
from_first = true
remove_redundant_aliases = true
profile = "black"

[tool.mypy]
ignore_missing_imports = true
check_untyped_defs = true
strict = true

[tool.pytest.ini_options]
addopts = "-W ignore::DeprecationWarning --cov=src/ --cov-fail-under=100"
trio_mode = true

[tool.pytest_env]
EC_RPS_SETTINGS_MODULE = "ec_rps_config_controller.settings.test"

[tool.coverage.report]
exclude_also = [
  "if TYPE_CHECKING:",
  "if __name__ == .__main__.:",
]
omit = [
  "src/ec_netbox_client/**/*.py",
  "src/ec_rps_config_controller/settings/prod.py",
  "src/ec_rps_config_controller/settings/__init__.py",
  "src/ec_rps_config_controller/__main__.py",
  "src/ec_rps_config_proxy_nginx/*.py",
  "src/ec_rps_config_waf_coraza/*.py",
  "src/ec_rps_config_waf_modsecurity/*.py",
]

[build-system]
requires = ["poetry-core"]
build-backend = "poetry.core.masonry.api"
