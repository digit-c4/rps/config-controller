from ec_rps_config_controller.utils.templates import get_template


def test_template_rendering(monkeypatch, proxy_template_context):
    config = {
        "EC_RPS_NGINX_PLUS_ENABLED": "true",
        "EC_RPS_MAX_CACHE_SIZE": "2g",
        "EC_RPS_TRUSTED_IPS": "1.2.3.4,5.6.7.8",
        "EC_RPS_SAML_ENABLED": "true",
        "EC_RPS_SAML_SP_ENTITY_ID": "https://entity-id",
        "EC_RPS_SAML_SP_ACS_URL": "https://acs-url",
        "EC_RPS_SAML_SP_REQUEST_BINDING": "HTTP-POST",
        "EC_RPS_SAML_SP_SIGN_AUTHN": "false",
        "EC_RPS_SAML_SP_SIGNING_KEY": "",
        "EC_RPS_SAML_SP_DECRYPTION_KEY": "",
        "EC_RPS_SAML_SP_FORCE_AUTHN": "false",
        "EC_RPS_SAML_SP_NAMEID_FORMAT": "urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified",
        "EC_RPS_SAML_SP_RELAY_STATE": "",
        "EC_RPS_SAML_SP_WANT_SIGNED_RESPONSE": "false",
        "EC_RPS_SAML_SP_WANT_SIGNED_ASSERTION": "false",
        "EC_RPS_SAML_SP_WANT_ENCRYPTED_ASSERTION": "false",
        "EC_RPS_SAML_SP_SLO_URL": "https://slo-url",
        "EC_RPS_SAML_SP_SLO_BINDING": "HTTP-POST",
        "EC_RPS_SAML_SP_SIGN_SLO": "false",
        "EC_RPS_SAML_SP_WANT_SIGNED_SLO": "false",
        "EC_RPS_SAML_IDP_ENTITY_ID": "https://idp-entity-id",
        "EC_RPS_SAML_IDP_SSO_URL": "https://idp-sso-url",
        "EC_RPS_SAML_IDP_VERIFICATION_CERTIFICATE": "",
        "EC_RPS_SAML_IDP_SLO_URL": "https://idp-slo-url",
        "EC_RPS_SAML_SP_REDIRECT_BASE": "https://redirect-base",
        "EC_RPS_SAML_AUTH_HEADER_NAME": "Remote-User",
    }

    for key, value in config.items():
        monkeypatch.setenv(key, value)

    template = get_template("ec_rps_config_proxy_nginx", "nginx.j2")
    template.render(proxy_template_context)
