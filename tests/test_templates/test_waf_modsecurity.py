from ec_rps_config_controller.utils.templates import get_template


def test_template_rendering(monkeypatch, waf_template_context):
    monkeypatch.setenv("EC_RPS_ACME_ENABLED", "true")
    monkeypatch.setenv("EC_RPS_PROXY_HOST", "proxy.ec.local")

    template = get_template("ec_rps_config_waf_modsecurity", "apache2.j2")
    template.render(waf_template_context)
