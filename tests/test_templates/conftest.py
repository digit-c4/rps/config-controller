from copy import deepcopy
from datetime import datetime

import pytest


@pytest.fixture
def template_context():
    return {
        "env_name": "test",
        "restricted_allowed_origins": None,
        "certs_path": "/path/to/certs",
        "rps_mappings": [
            dict(
                id=1,
                url="<netbox entity url>",
                source="https://example1.test.bubble.net",
                target="https://example.com",
                created=datetime.now(),
                last_updated=datetime.now(),
                http_headers=[],
                acl_denied_source=[],
                cache_configs=[],
                hsts_protocol=dict(
                    max_age=31_536_000,
                    id=1,
                    url="<netbox entity url>",
                    mapping=1,
                    subdomains=True,
                    preload_flag=True,
                    last_updated=datetime.now(),
                ),
                authentication=None,
                testingpage=None,
                webdav=False,
                comment="",
                gzip_proxied=False,
                keepalive_requests=100,
                keepalive_timeout=75,
                proxy_cache=False,
                proxy_read_timeout=60,
                client_max_body_size=2048,
                extra_protocols=[],
                sorry_page="",
                proxy_buffer_size=4,
                proxy_buffer=16,
                proxy_busy_buffer=8,
                proxy_buffer_requests=True,
                proxy_buffer_responses=True,
            ),
            dict(
                id=2,
                url="<netbox entity url>",
                source="https://example2.test.bubble.net",
                target="https://example.com",
                created=datetime.now(),
                last_updated=datetime.now(),
                http_headers=[],
                acl_denied_source=[],
                cache_configs=[],
                hsts_protocol=dict(
                    max_age=31_536_000,
                    id=2,
                    url="<netbox entity url>",
                    mapping=2,
                    subdomains=False,
                    preload_flag=False,
                    last_updated=datetime.now(),
                ),
                authentication=None,
                testingpage=None,
                webdav=False,
                comment="",
                gzip_proxied=False,
                keepalive_requests=100,
                keepalive_timeout=75,
                proxy_cache=False,
                proxy_read_timeout=60,
                client_max_body_size=2048,
                extra_protocols=[],
                sorry_page="",
                proxy_buffer_size=4,
                proxy_buffer=16,
                proxy_busy_buffer=8,
                proxy_buffer_requests=True,
                proxy_buffer_responses=True,
            ),
        ],
    }


@pytest.fixture
def proxy_template_context(template_context):
    ctx = deepcopy(template_context)
    ctx.update(
        {
            "nginx_plus_enabled": True,
            "saml_enabled": False,
            "max_cache_size": "2g",
            "trusted_ips": ["1.2.3.4"],
        }
    )
    return ctx


@pytest.fixture
def waf_template_context(template_context):
    ctx = deepcopy(template_context)
    ctx.update(
        {
            "acme_enabled": True,
            "proxy_host": "proxy.ec.local",
            "proxy_protocol": False,
            "upstream_has_tls": False,
        }
    )
    return ctx
