from unittest.mock import MagicMock

import pytest


@pytest.fixture
def mapping_data():
    return {
        "id": 1,
        "url": "mock://netbox/api/plugins/rps/mappings/1/",
        "created": None,
        "last_updated": None,
        "source": "https://example.com",
        "target": "http://localhost:8080",
        "http_headers": [],
        "extra_protocols": [],
        "acl_denied_source": [],
        "cache_configs": [],
        "hsts_protocol": None,
        "client_max_body_size": 1,
        "keepalive_timeout": 75,
        "keepalive_requests": 1000,
        "proxy_cache": False,
        "proxy_read_timeout": 60,
        "gzip_proxied": False,
        "sorry_page": "https://example.com/sorry",
    }


async def test_webhook_create(http_client, controller_world, mapping_data):
    response = await http_client.post(
        "/webhook/netbox",
        json={
            "event": "created",
            "timestamp": "2021-01-01T00:00:00Z",
            "model": "mapping",
            "username": "test",
            "request_id": "request-1",
            "data": mapping_data,
            "snapshots": {
                "prechange": None,
                "postchange": None,
            },
        },
    )

    assert response.status_code == 200, response.text
    assert response.json() == {"success": True}, response.text

    mapping = dict(id=1)

    assert controller_world["requests"][0][0] == "changed"
    assert controller_world["requests"][0][1]["id"] == 1


async def test_webhook_update(http_client, controller_world, mapping_data):
    response = await http_client.post(
        "/webhook/netbox",
        json={
            "event": "updated",
            "timestamp": "2021-01-01T00:00:00Z",
            "model": "mapping",
            "username": "test",
            "request_id": "request-1",
            "data": mapping_data,
            "snapshots": {
                "prechange": None,
                "postchange": None,
            },
        },
    )

    assert response.status_code == 200, response.text
    assert response.json() == {"success": True}, response.text

    mapping = dict(id=1)

    assert controller_world["requests"][0][0] == "changed"
    assert controller_world["requests"][0][1]["id"] == 1


async def test_webhook_delete(http_client, controller_world, mapping_data):
    response = await http_client.post(
        "/webhook/netbox",
        json={
            "event": "deleted",
            "timestamp": "2021-01-01T00:00:00Z",
            "model": "mapping",
            "username": "test",
            "request_id": "request-1",
            "data": mapping_data,
            "snapshots": {
                "prechange": None,
                "postchange": None,
            },
        },
    )

    assert response.status_code == 200, response.text
    assert response.json() == {"success": True}, response.text

    mapping = dict(id=1)

    assert controller_world["requests"][0][0] == "deleted"
    assert controller_world["requests"][0][1]["id"] == 1


async def test_webhook_invalid(http_client, mapping_data):
    response = await http_client.post(
        "/webhook/netbox",
        json={
            "event": "invalid",
            "timestamp": "2021-01-01T00:00:00Z",
            "model": "mapping",
            "username": "test",
            "request_id": "request-1",
            "data": mapping_data,
            "snapshots": {
                "prechange": None,
                "postchange": None,
            },
        },
    )

    assert response.status_code == 400, response.text


async def test_webhook_failure(http_client, controller_world, mapping_data):
    controller_world["res"] = RuntimeError("test")

    response = await http_client.post(
        "/webhook/netbox",
        json={
            "event": "created",
            "timestamp": "2021-01-01T00:00:00Z",
            "model": "mapping",
            "username": "test",
            "request_id": "request-1",
            "data": mapping_data,
            "snapshots": {
                "prechange": None,
                "postchange": None,
            },
        },
    )

    assert response.status_code == 500, response.text
    assert response.json() == {"detail": "test"}, response.text
