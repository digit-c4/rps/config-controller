async def test_auth(http_client):
    response = await http_client.get("/health")
    assert response.status_code == 200


async def test_noauth(http_client_noauth):
    response = await http_client_noauth.get("/health")
    assert response.status_code == 403


async def test_wrongauth(http_client_wrongauth):
    response = await http_client_wrongauth.get("/health")
    assert response.status_code == 403
