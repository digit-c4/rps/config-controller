async def test_healthcheck(http_client):
    response = await http_client.get("/health")
    assert response.status_code == 200
    assert response.json() == {"status": "ok", "detail": None}
