import hashlib
import hmac

from httpx import AsyncClient, Request
import pytest
import trio

from ec_rps_config_controller import settings
from ec_rps_config_controller.api import asgi_app
from ec_rps_config_controller.utils.gen_server import gen_server


@gen_server()
async def mock_controller(chan, world, task_status=trio.TASK_STATUS_IGNORED):
    task_status.started()

    async for respond_to, msg in chan:
        world["requests"].append(msg)
        await respond_to.send(world["res"])
        await respond_to.aclose()


@pytest.fixture
def controller_world():
    yield {"requests": [], "res": None}


@pytest.fixture
async def http_client(controller_world):
    shutdown_event = trio.Event()

    def shutdown():
        shutdown_event.set()

    async def shutdown_trigger():
        await shutdown_event.wait()

    async with trio.open_nursery() as nursery:
        chan = await nursery.start(mock_controller, controller_world)
        binds = await nursery.start(asgi_app, chan, shutdown_trigger)
        assert len(binds) >= 1

        async def sign_request(request: Request):
            content = await request.aread()
            signature = hmac.new(
                key=settings.EC_RPS_SECRET.encode(),
                msg=content,
                digestmod=hashlib.sha512,
            ).hexdigest()
            request.headers["X-Hook-Signature"] = signature

        async with AsyncClient(
            base_url=binds[0],
            trust_env=False,
            event_hooks={"request": [sign_request]},
        ) as client:
            yield client

        shutdown()


@pytest.fixture
async def http_client_noauth(controller_world):
    shutdown_event = trio.Event()

    def shutdown():
        shutdown_event.set()

    async def shutdown_trigger():
        await shutdown_event.wait()

    async with trio.open_nursery() as nursery:
        chan = await nursery.start(mock_controller, controller_world)
        binds = await nursery.start(asgi_app, chan, shutdown_trigger)
        assert len(binds) >= 1

        async with AsyncClient(
            base_url=binds[0],
            trust_env=False,
        ) as client:
            yield client

        shutdown()


@pytest.fixture
async def http_client_wrongauth(controller_world):
    shutdown_event = trio.Event()

    def shutdown():
        shutdown_event.set()

    async def shutdown_trigger():
        await shutdown_event.wait()

    async with trio.open_nursery() as nursery:
        chan = await nursery.start(mock_controller, controller_world)
        binds = await nursery.start(asgi_app, chan, shutdown_trigger)
        assert len(binds) >= 1

        headers = {
            "X-Hook-Signature": "invalid",
        }

        async with AsyncClient(
            base_url=binds[0],
            headers=headers,
            trust_env=False,
        ) as client:
            yield client

        shutdown()
