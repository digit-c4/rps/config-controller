async def test_dump_current(http_client, controller_world):
    controller_world["res"] = "test"

    response = await http_client.get("/debug/config/current")
    assert response.status_code == 200
    assert response.text == "test"


async def test_dump_current_failure(http_client, controller_world):
    controller_world["res"] = RuntimeError("test")

    response = await http_client.get("/debug/config/current")
    assert response.status_code == 500


async def test_dump_desired(http_client, controller_world):
    controller_world["res"] = "test"

    response = await http_client.get("/debug/config/desired")
    assert response.status_code == 200
    assert response.text == "test"


async def test_dump_desired_failure(http_client, controller_world):
    controller_world["res"] = RuntimeError("test")

    response = await http_client.get("/debug/config/desired")
    assert response.status_code == 500
