from unittest.mock import AsyncMock, MagicMock, patch

from ec_rps_config_controller.business.webhook import setup_webhook


@patch("ec_rps_config_controller.business.webhook.gethostname")
@patch("ec_rps_config_controller.business.webhook.ECNetboxClient")
@patch("ec_rps_config_controller.business.webhook.extras_webhooks_list")
@patch("ec_rps_config_controller.business.webhook.extras_webhooks_create")
@patch("ec_rps_config_controller.business.webhook.extras_webhooks_bulk_destroy")
async def test_setup_webhook(
    mocked_api_destroy,
    mocked_api_create,
    mocked_api_list,
    mocked_client_class,
    mocked_gethostname,
):
    mocked_gethostname.return_value = "test.ec.local"

    client = AsyncMock()
    client.__aenter__.return_value = client
    mocked_client_class.return_value = client

    webhook_a = MagicMock()

    mocked_api_list.return_value = dict(results=[webhook_a])

    await setup_webhook()

    mocked_api_list.assert_called_once()
    mocked_api_destroy.assert_called_once()
    mocked_api_create.assert_called_once()
