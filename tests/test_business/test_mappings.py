from unittest.mock import AsyncMock, MagicMock, patch

import pytest

from ec_rps_config_controller.business.mappings import fetch_mappings


@patch("ec_rps_config_controller.business.mappings.plugins_rps_mapping_list")
async def test_fetch_mappings(mocked_api):
    mapping = dict(id=1)

    mocked_api.return_value = dict(results=[mapping])

    res = await fetch_mappings()
    assert len(res) == 1
    assert res[0] == mapping

    mocked_api.assert_called_once()


@patch("ec_rps_config_controller.business.mappings.plugins_rps_mapping_list")
async def test_fetch_mappings_failure(mocked_api):
    mocked_api.side_effect = RuntimeError("test")

    with pytest.raises(RuntimeError, match="test"):
        await fetch_mappings()

    assert mocked_api.call_count == 5
