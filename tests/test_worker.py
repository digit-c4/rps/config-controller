from unittest.mock import patch

import trio

from ec_rps_config_controller.utils.gen_server import gen_server
from ec_rps_config_controller.worker import ShutdownHandler, task


@gen_server()
async def mock_sync_task(chan_rx, task_status=trio.TASK_STATUS_IGNORED):
    task_status.started()

    async for respond_to, _req in chan_rx:
        await respond_to.send(None)
        await respond_to.aclose()


@patch("ec_rps_config_controller.worker.sync.task", new=mock_sync_task)
@patch("ec_rps_config_controller.worker.setup_webhook")
async def test_worker(mock_setup_webhook):
    with trio.fail_after(5):
        async with trio.open_nursery() as nursery:
            shutdown_handler = ShutdownHandler()

            nursery.start_soon(task, shutdown_handler.wait)
            await trio.sleep(0.1)

            shutdown_handler.trigger()

    mock_setup_webhook.assert_called_once()
