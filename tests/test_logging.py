from unittest.mock import MagicMock

from freezegun import freeze_time
from hypercorn import Config
from logbook import Handler, Logger
from logbook import TestHandler as LogbookTestHandler
from logbook import set_datetime_format
import pytest

from ec_rps_config_controller import logging


@pytest.fixture
def loghandler():
    handler = LogbookTestHandler()
    handler.formatter = logging.logfmt_formatter

    with freeze_time("2021-01-01 12:00:00") as frozen_datetime:
        set_datetime_format(frozen_datetime)

        with handler.applicationbound():
            yield handler


@pytest.fixture
def logger(loghandler):
    yield Logger("test")


@pytest.fixture
def weblogger():
    yield logging.WebLogger(Config())


def test_logfmt(logger, loghandler):
    logger.info("Hello, world!")
    assert (
        loghandler.formatted_records[0]
        == 'app="ec_rps_config_controller" bubble="test" time="2021-01-01 12:00:00" level="INFO" message="Hello, world!"'
    )


def test_log_exception(logger, loghandler):
    try:
        raise RuntimeError("test")

    except RuntimeError as err:
        logger.exception("error")

    assert 'exc.type="RuntimeError"' in loghandler.formatted_records[0]
    assert 'exc.message="test"' in loghandler.formatted_records[0]
    assert 'message="error"' in loghandler.formatted_records[0]


def test_create_loghandler():
    handler = logging.create_loghandler()
    assert isinstance(handler, Handler)


async def test_weblogger_access(loghandler, weblogger):
    def make_req():
        req = MagicMock()
        req.__getitem__.side_effect = lambda x: getattr(req, x, None)
        req.get.side_effect = lambda x, y=None: getattr(req, x, y)
        return req

    req = make_req()
    req.client = None
    await weblogger.access(req, MagicMock(), 0.0)
    assert loghandler.records[0].extra["scope"] == "api"
    assert loghandler.records[0].extra["remote_addr"] is None
    assert loghandler.records[0].extra["method"] == "GET"
    loghandler.records.clear()

    req = make_req()
    req.client = ("host", 1234)
    await weblogger.access(req, MagicMock(), 0.0)
    assert loghandler.records[0].extra["scope"] == "api"
    assert loghandler.records[0].extra["remote_addr"] == "host:1234"
    assert loghandler.records[0].extra["method"] == "GET"
    loghandler.records.clear()

    req = make_req()
    req.client = ("addr",)
    await weblogger.access(req, MagicMock(), 0.0)
    assert loghandler.records[0].extra["scope"] == "api"
    assert loghandler.records[0].extra["remote_addr"] == "addr"
    assert loghandler.records[0].extra["method"] == "GET"
    loghandler.records.clear()

    req = make_req()
    req.client = "client"
    await weblogger.access(req, MagicMock(), 0.0)
    assert loghandler.records[0].extra["scope"] == "api"
    assert loghandler.records[0].extra["remote_addr"] == "<???client???>"
    assert loghandler.records[0].extra["method"] == "GET"
    loghandler.records.clear()

    req = make_req()
    req.type = "http"
    req.method = "POST"
    await weblogger.access(req, MagicMock(), 0.0)
    assert loghandler.records[0].extra["scope"] == "api"
    assert loghandler.records[0].extra["method"] == "POST"
    loghandler.records.clear()

    req = make_req()
    await weblogger.access(req, None, 0.0)
    assert len(loghandler.records) == 0


async def test_weblogger_critical(weblogger, loghandler):
    await weblogger.critical("Critical message")
    assert loghandler.records[0].level_name == "CRITICAL"
    assert loghandler.records[0].message == "Critical message"
    assert loghandler.records[0].extra["scope"] == "api"


async def test_weblogger_error(weblogger, loghandler):
    await weblogger.error("Error message")
    assert loghandler.records[0].level_name == "ERROR"
    assert loghandler.records[0].message == "Error message"
    assert loghandler.records[0].extra["scope"] == "api"


async def test_weblogger_warning(weblogger, loghandler):
    await weblogger.warning("Warning message")
    assert loghandler.records[0].level_name == "WARNING"
    assert loghandler.records[0].message == "Warning message"
    assert loghandler.records[0].extra["scope"] == "api"


async def test_weblogger_info(weblogger, loghandler):
    await weblogger.info("Info message")
    assert loghandler.records[0].level_name == "INFO"
    assert loghandler.records[0].message == "Info message"
    assert loghandler.records[0].extra["scope"] == "api"


async def test_weblogger_debug(weblogger, loghandler):
    await weblogger.debug("Debug message")
    assert loghandler.records[0].level_name == "DEBUG"
    assert loghandler.records[0].message == "Debug message"
    assert loghandler.records[0].extra["scope"] == "api"


async def test_weblogger_exception(weblogger, loghandler):
    try:
        raise RuntimeError("Exception message")

    except RuntimeError as err:
        await weblogger.exception(f"{err}")
        assert loghandler.records[0].level_name == "ERROR"
        assert loghandler.records[0].message == "Exception message"
        assert loghandler.records[0].extra["scope"] == "api"
