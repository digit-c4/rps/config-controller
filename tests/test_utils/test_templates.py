from ec_rps_config_controller.utils.templates import get_template


def test_get_template():
    template = get_template("ec_rps_config_proxy_nginx", "nginx.j2")
    assert template is not None
