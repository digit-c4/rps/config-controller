import pytest
import trio

from ec_rps_config_controller.utils.gen_server import gen_server


async def pingpong_client(id, chan, world):
    world["response"][id] = await chan.call((id, "ping"))
    await chan.close()


async def echo_client(id, chan):
    try:
        await chan.call((id, "echo"))

    except Exception as err:
        await chan.close()
        raise err


@gen_server()
async def pingpong_server(chan, world, task_status=trio.TASK_STATUS_IGNORED):
    task_status.started()

    async for respond_to, payload in chan:
        id, msg = payload
        world["request"][id] = msg

        if msg == "ping":
            await respond_to.send("pong")

        else:
            await respond_to.send(NotImplementedError(msg))

        await respond_to.aclose()


async def test_gen_server():
    world = {"request": {}, "response": {}}

    async with trio.open_nursery() as nursery:
        chan = await nursery.start(pingpong_server, world)
        nursery.start_soon(pingpong_client, "foo", chan.clone(), world)
        nursery.start_soon(pingpong_client, "bar", chan.clone(), world)
        await chan.close()

    assert world["request"]["foo"] == "ping"
    assert world["request"]["bar"] == "ping"

    assert world["response"]["foo"] == "pong"
    assert world["response"]["bar"] == "pong"


async def test_channel_error():
    world = {"request": {}}

    with trio.testing.RaisesGroup(
        trio.testing.Matcher(NotImplementedError, match="echo"),
    ):
        async with trio.open_nursery() as nursery:
            chan = await nursery.start(pingpong_server, world)
            nursery.start_soon(echo_client, "foo", chan)

    assert world["request"]["foo"] == "echo"
