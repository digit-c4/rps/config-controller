from datetime import UTC, datetime, timedelta
from unittest.mock import AsyncMock, MagicMock

from cryptography import x509
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.asymmetric import rsa

from ec_rps_config_controller.utils import x509 as x509_utils


async def test_self_signed_generation():
    key_path = MagicMock()
    key_path.write_bytes = AsyncMock()
    cert_path = MagicMock()
    cert_path.write_bytes = AsyncMock()
    common_name = "test"

    await x509_utils.self_signed(key_path, cert_path, common_name)

    key_path.write_bytes.assert_called()
    cert_path.write_bytes.assert_called()


async def test_check_certificate_pair_match():
    test_key = rsa.generate_private_key(public_exponent=65537, key_size=2048)
    test_subject = test_issuer = x509.Name(
        [
            x509.NameAttribute(x509.NameOID.COMMON_NAME, "test.ec.local"),
            x509.NameAttribute(x509.NameOID.ORGANIZATION_NAME, "European Commission"),
        ]
    )

    test_cert = (
        x509.CertificateBuilder()
        .subject_name(test_subject)
        .issuer_name(test_issuer)
        .public_key(test_key.public_key())
        .serial_number(x509.random_serial_number())
        .not_valid_before(datetime.now(UTC))
        .not_valid_after(datetime.now(UTC) + timedelta(days=365))
        .sign(test_key, hashes.SHA256())
    )

    test_key_bytes = test_key.private_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PrivateFormat.TraditionalOpenSSL,
        encryption_algorithm=serialization.NoEncryption(),
    )

    test_cert_bytes = test_cert.public_bytes(serialization.Encoding.PEM)

    key_path = MagicMock()
    key_path.read_bytes = AsyncMock()
    key_path.read_bytes.return_value = test_key_bytes
    cert_path = MagicMock()
    cert_path.read_bytes = AsyncMock()
    cert_path.read_bytes.return_value = test_cert_bytes

    matched = await x509_utils.check_certificate_pair_match(key_path, cert_path)
    assert matched, "The public and private keys should match."

    key_path.read_bytes.assert_called()
    cert_path.read_bytes.assert_called()


async def test_check_certificate_pair_mismatch():
    key_path = MagicMock()
    key_path.read_bytes = AsyncMock()
    key_path.read_bytes.return_value = b"key"
    cert_path = MagicMock()
    cert_path.read_bytes = AsyncMock()
    cert_path.read_bytes.return_value = b"cert"

    matched = await x509_utils.check_certificate_pair_match(key_path, cert_path)
    assert not matched, "The public and private keys should not match."

    key_path.read_bytes.assert_called()
    cert_path.read_bytes.assert_called()
