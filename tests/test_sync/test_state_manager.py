from unittest.mock import AsyncMock, patch

import pytest
import trio

from ec_rps_config_controller.sync import state_manager


async def test_state_manager():
    mapping_id = 1
    mapping_t0 = dict(
        id=mapping_id,
        url="mock://netbox/api/plugins/rps/mappings/1/",
        created=None,
        last_updated=None,
        source="source-t0",
        target="target-t0",
        http_headers=[],
        saml_config=None,
        acl_denied_source=[],
        cache_configs=[],
        hsts_protocol=None,
    )

    mapping_t1 = dict(
        id=mapping_id,
        url="mock://netbox/api/plugins/rps/mappings/1/",
        created=None,
        last_updated=None,
        source="source-t1",
        target="target-t0",
        http_headers=[],
        saml_config=None,
        acl_denied_source=[],
        cache_configs=[],
        hsts_protocol=None,
    )

    async with trio.open_nursery() as nursery:
        chan = await nursery.start(state_manager.task)

        res = await chan.call(("list",))
        assert len(res) == 0

        res = await chan.call(("put", mapping_t0))
        assert res is None

        res = await chan.call(("list",))
        assert len(res) == 1
        assert res[0]["id"] == mapping_id
        assert res[0]["source"] == mapping_t0["source"]

        res = await chan.call(("get", mapping_id))
        assert res is not None
        assert res["id"] == mapping_id

        res = await chan.call(("get", 2))
        assert res is None

        res = await chan.call(("put", mapping_t1))
        assert res is not None
        assert res["id"] == mapping_id
        assert res["source"] == mapping_t0["source"]

        res = await chan.call(("del", mapping_id))
        assert res is not None
        assert res["id"] == mapping_id
        assert res["source"] == mapping_t1["source"]

        res = await chan.call(("list",))
        assert len(res) == 0

        res = await chan.call(("del", mapping_id))
        assert res is None

        with pytest.raises(NotImplementedError):
            await chan.call("invalid")

        await chan.close()


async def test_state_manager_write():
    mapping = dict(
        id=1,
        url="mock://netbox/api/plugins/rps/mappings/1/",
        created=None,
        last_updated=None,
        source="https://example.com",
        target="http://localhost:8080",
        saml_config=None,
        http_headers=[],
        extra_protocols=[],
        acl_denied_source=[],
        cache_configs=[],
        hsts_protocol=None,
        client_max_body_size=1,
        keepalive_timeout=75,
        keepalive_requests=1000,
        proxy_cache=False,
        proxy_read_timeout=60,
        gzip_proxied=False,
        sorry_page="https://example.com/sorry",
    )

    async with trio.open_nursery() as nursery:
        chan = await nursery.start(state_manager.task)

        await chan.call(("put", mapping))

        res = await chan.call(("write",))
        assert res is None

        await chan.close()


@patch("ec_rps_config_controller.sync.state_manager.settings")
async def test_state_manager_write_failure(settings_mock):
    settings_mock.EC_RPS_CONFIG_MODULE.render = AsyncMock(
        side_effect=RuntimeError("test"),
    )

    async with trio.open_nursery() as nursery:
        chan = await nursery.start(state_manager.task)

        with pytest.raises(RuntimeError, match="test"):
            await chan.call(("write",))

        await chan.close()


@patch("ec_rps_config_controller.sync.state_manager.settings")
async def test_state_manager_dump_current(settings_mock):
    settings_mock.EC_RPS_CONFIG_MODULE.dump = AsyncMock()
    settings_mock.EC_RPS_CONFIG_MODULE.dump.return_value = "test"

    async with trio.open_nursery() as nursery:
        chan = await nursery.start(state_manager.task)

        resp = await chan.call(("dump", "current"))
        assert resp == "test"

        await chan.close()


@patch("ec_rps_config_controller.sync.state_manager.settings")
async def test_state_manager_dump_current_failure(settings_mock):
    settings_mock.EC_RPS_CONFIG_MODULE.dump = AsyncMock(
        side_effect=RuntimeError("test"),
    )

    async with trio.open_nursery() as nursery:
        chan = await nursery.start(state_manager.task)

        with pytest.raises(RuntimeError, match="test"):
            await chan.call(("dump", "current"))

        await chan.close()


@patch("ec_rps_config_controller.sync.state_manager.settings")
async def test_state_manager_dump_desired(settings_mock):
    settings_mock.EC_RPS_CONFIG_MODULE.render = AsyncMock()
    settings_mock.EC_RPS_CONFIG_MODULE.render.return_value = "test"

    async with trio.open_nursery() as nursery:
        chan = await nursery.start(state_manager.task)

        resp = await chan.call(("dump", "desired"))
        assert resp == "test"

        await chan.close()


@patch("ec_rps_config_controller.sync.state_manager.settings")
async def test_state_manager_dump_desired_failure(settings_mock):
    settings_mock.EC_RPS_CONFIG_MODULE.render = AsyncMock(
        side_effect=RuntimeError("test"),
    )

    async with trio.open_nursery() as nursery:
        chan = await nursery.start(state_manager.task)

        with pytest.raises(RuntimeError, match="test"):
            await chan.call(("dump", "desired"))

        await chan.close()
