from unittest.mock import MagicMock

import pytest
import trio

from ec_rps_config_controller.sync import controller
from ec_rps_config_controller.utils.gen_server import gen_server


@gen_server()
async def mock_state_manager(chan, world, task_status=trio.TASK_STATUS_IGNORED):
    task_status.started()

    async for respond_to, req in chan:
        world["state_requests"].append(req)
        await respond_to.send(world.get("state_exc", world.get("state_resp")))
        await respond_to.aclose()


async def test_controller():
    world = {
        "state_requests": [],
    }

    mapping = dict(
        id=1,
        source="http://sub.example.com",
    )

    async with trio.open_nursery() as nursery:
        state_chan = await nursery.start(mock_state_manager, world)
        chan = await nursery.start(controller.task, state_chan)

        res = await chan.call(("changed", mapping))
        assert res is None

        res = await chan.call(("deleted", mapping))
        assert res is None

        world["state_resp"] = "test"
        res = await chan.call(("dump", "current"))
        assert res == "test"

        res = await chan.call(("dump", "desired"))
        assert res == "test"

        with pytest.raises(NotImplementedError):
            await chan.call("invalid")

        await chan.close()

    assert world["state_requests"][0] == ("write",)
    assert world["state_requests"][1] == ("put", mapping)
    assert world["state_requests"][2] == ("write",)
    assert world["state_requests"][3] == ("del", 1)
    assert world["state_requests"][4] == ("write",)
    assert world["state_requests"][5] == ("dump", "current")
    assert world["state_requests"][6] == ("dump", "desired")
    assert len(world["state_requests"]) == 7


async def test_controller_state_failure():
    world = {
        "state_requests": [],
    }

    mapping = dict(
        id=1,
        source="http://example.com",
    )

    async with trio.open_nursery() as nursery:
        state_chan = await nursery.start(mock_state_manager, world)
        chan = await nursery.start(controller.task, state_chan)

        world["state_exc"] = RuntimeError("test")

        with pytest.raises(RuntimeError, match="test"):
            await chan.call(("changed", mapping))

        with pytest.raises(RuntimeError, match="test"):
            await chan.call(("deleted", mapping))

        with pytest.raises(RuntimeError, match="test"):
            await chan.call(("dump", "current"))

        with pytest.raises(RuntimeError, match="test"):
            await chan.call(("dump", "desired"))

        await chan.close()

    assert world["state_requests"][0] == ("write",)
    assert world["state_requests"][1] == ("put", mapping)
    assert world["state_requests"][2] == ("del", 1)
    assert world["state_requests"][3] == ("dump", "current")
    assert world["state_requests"][4] == ("dump", "desired")
    assert len(world["state_requests"]) == 5
