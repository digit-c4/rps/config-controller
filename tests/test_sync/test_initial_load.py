from unittest.mock import AsyncMock, MagicMock, patch

import trio

from ec_rps_config_controller import sync
from ec_rps_config_controller.utils.gen_server import gen_server


@gen_server()
async def mock_state_manager(chan, task_status=trio.TASK_STATUS_IGNORED):
    task_status.started()

    async for respond_to, req in chan:
        await respond_to.send(None)
        await respond_to.aclose()


def make_mock_controller(requests, err=False):
    @gen_server()
    async def mock_controller(
        chan,
        state_chan,
        task_status=trio.TASK_STATUS_IGNORED,
    ):
        task_status.started()

        async for respond_to, req in chan:
            if err:
                await respond_to.send(Exception("error"))

            else:
                requests.append(req)
                await respond_to.send(None)

            await respond_to.aclose()

        await state_chan.close()

    return mock_controller


async def mock_filewatcher(state_chan, task_status=trio.TASK_STATUS_IGNORED):
    task_status.started()
    await state_chan.close()


@patch("ec_rps_config_controller.business.mappings.plugins_rps_mapping_list")
@patch("ec_rps_config_controller.sync.controller.task")
@patch("ec_rps_config_controller.sync.state_manager.task")
@patch("ec_rps_config_controller.sync.filewatcher.task")
async def test_initial_load(
    filewatcher_task,
    state_manager_task,
    controller_task,
    mocked_api,
):
    requests = []
    state_manager_task.side_effect = mock_state_manager
    filewatcher_task.side_effect = mock_filewatcher
    controller_task.side_effect = make_mock_controller(requests)

    mapping = dict(id=1)

    mocked_api.return_value = dict(results=[mapping])

    async with trio.open_nursery() as nursery:
        controller_chan = await nursery.start(sync.task)
        await controller_chan.close()

    assert len(requests) == 1
    assert requests[0] == ("changed", mapping)


@patch("ec_rps_config_controller.business.mappings.plugins_rps_mapping_list")
@patch("ec_rps_config_controller.sync.controller.task")
@patch("ec_rps_config_controller.sync.state_manager.task")
@patch("ec_rps_config_controller.sync.filewatcher.task")
async def test_initial_load_empty(
    filewatcher_task,
    state_manager_task,
    controller_task,
    mocked_api,
):
    requests = []
    state_manager_task.side_effect = mock_state_manager
    filewatcher_task.side_effect = mock_filewatcher
    controller_task.side_effect = make_mock_controller(requests)

    mocked_api.return_value = dict(results=[])

    async with trio.open_nursery() as nursery:
        controller_chan = await nursery.start(sync.task)
        await controller_chan.close()

    assert len(requests) == 0


@patch("ec_rps_config_controller.business.mappings.plugins_rps_mapping_list")
@patch("ec_rps_config_controller.sync.controller.task")
@patch("ec_rps_config_controller.sync.state_manager.task")
@patch("ec_rps_config_controller.sync.filewatcher.task")
async def test_initial_load_failure(
    filewatcher_task,
    state_manager_task,
    controller_task,
    mocked_api,
):
    requests = []
    state_manager_task.side_effect = mock_state_manager
    filewatcher_task.side_effect = mock_filewatcher
    controller_task.side_effect = make_mock_controller(requests, err=True)

    mapping = dict(id=1)

    mocked_api.return_value = dict(results=[mapping])

    async with trio.open_nursery() as nursery:
        controller_chan = await nursery.start(sync.task)
        await controller_chan.close()

    assert len(requests) == 0
