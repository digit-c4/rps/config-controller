from unittest.mock import AsyncMock, MagicMock, patch

import trio

from ec_rps_config_controller.sync import filewatcher
from ec_rps_config_controller.utils.gen_server import ChannelWriter


async def test_notifier():
    async def producer(notifier):
        event = MagicMock()
        event.event_type = "modified"
        await trio.to_thread.run_sync(notifier.on_any_event, event)

    async def consumer(channel, task_status=trio.TASK_STATUS_IGNORED):
        respond_to, request = await channel.receive()
        task_status.started(request)
        await respond_to.send(None)
        await respond_to.aclose()

    chan_tx, chan_rx = trio.open_memory_channel(0)
    state_chan_tx = ChannelWriter(chan_tx)
    notifier = filewatcher.NotifyStateManager(state_chan_tx)

    async with trio.open_nursery() as nursery:
        nursery.start_soon(producer, notifier)
        msg = await nursery.start(consumer, chan_rx)
        await state_chan_tx.close()

    assert msg == ("write",)


async def test_notifier_failure():
    async def producer(notifier):
        event = MagicMock()
        event.event_type = "modified"
        await trio.to_thread.run_sync(notifier.on_any_event, event)

    async def consumer(channel, task_status=trio.TASK_STATUS_IGNORED):
        respond_to, request = await channel.receive()
        task_status.started(request)
        await respond_to.send(Exception("oops"))
        await respond_to.aclose()

    chan_tx, chan_rx = trio.open_memory_channel(0)
    state_chan_tx = ChannelWriter(chan_tx)
    notifier = filewatcher.NotifyStateManager(state_chan_tx)

    async with trio.open_nursery() as nursery:
        nursery.start_soon(producer, notifier)
        msg = await nursery.start(consumer, chan_rx)
        await state_chan_tx.close()

    assert msg == ("write",)


@patch("ec_rps_config_controller.sync.filewatcher.Observer")
async def test_thread_worker(observer_mock_class):
    observer_mock = observer_mock_class.return_value
    observer_mock.schedule.return_value = None

    mock_channel = AsyncMock()
    started_event = trio.Event()
    shutdown_event = trio.Event()

    async with trio.open_nursery() as nursery:
        nursery.start_soon(
            trio.to_thread.run_sync,
            filewatcher.thread_worker,
            mock_channel,
            started_event,
            shutdown_event,
        )
        await started_event.wait()
        shutdown_event.set()

    observer_mock.schedule.assert_called_once()
    observer_mock.start.assert_called_once()
    observer_mock.stop.assert_called_once()
    observer_mock.join.assert_called_once()


@patch("ec_rps_config_controller.sync.filewatcher.Observer")
async def test_task(observer_mock_class):
    observer_mock = observer_mock_class.return_value
    observer_mock.schedule.return_value = None

    mock_channel = AsyncMock()
    mock_channel.close.return_value = None

    async with trio.open_nursery() as nursery:
        await nursery.start(
            filewatcher.task,
            mock_channel,
        )
        nursery.cancel_scope.cancel()

    observer_mock.schedule.assert_called_once()
    observer_mock.start.assert_called_once()
    observer_mock.stop.assert_called_once()
    observer_mock.join.assert_called_once()
    mock_channel.close.assert_called_once()
