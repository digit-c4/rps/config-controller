# Config Controller

This repository contains the RPS Config Controller, a tool used to generate
configuration files from Netbox RPS Mappings.

For more information:

 - the [CONTRIBUTING](./CONTRIBUTING.md) document describes how to contribute to the repository
 - consult the [documentation](https://digit-c4.pages.code.europa.eu/rps/config-controller)
